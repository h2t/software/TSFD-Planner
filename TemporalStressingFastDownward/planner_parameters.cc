#include "planner_parameters.h"
#include <iostream>
#include <stdio.h>

using namespace tsfd;

PlannerParameters::PlannerParameters()
{
    // set defaults
    anytime_search = false;
    timeout_while_no_plan_found = 0;
    timeout_if_plan_found = 0;

    greedy = false;
    lazy_evaluation = false;
    verbose = true;

    open_list_initial_boost = 500;
    open_list_boost = 100;

    cyclic_cg_heuristic = false;
    cyclic_cg_preferred_operators = false;
    makespan_heuristic = false;
    makespan_heuristic_preferred_operators = false;
    no_heuristic = false;
    rtpg_heuristic = false;
    rtpg_heuristic_preferred_operators = false;
    known_plan_heuristic = false;

    cg_heuristic_zero_cost_waiting_transitions = true;
    cg_heuristic_fire_waiting_transitions_only_if_local_problems_matches_state = false;

    discretizations = 1;
    future_scheduling = false;
    past_scheduling = false;

    g_values = GMakespan;

    epsilonize_internally = false;
    epsilonize_externally = false;

    pref_ops_ordered_mode = false;
    pref_ops_cheapest_mode = false;
    pref_ops_most_expensive_mode = false;
    pref_ops_rand_mode = false;
    pref_ops_concurrent_mode = false;
    number_pref_ops_ordered_mode = 1000;
    number_pref_ops_cheapest_mode = 1000;
    number_pref_ops_most_expensive_mode = 1000;
    number_pref_ops_rand_mode = 1000;

    planMonitorFileName = "";

    monitoring_verify_timestamps = false;

    max_makespan = HUGE_VAL;
    max_cost = HUGE_VAL;

    costCalculation = ADD;
}

PlannerParameters::~PlannerParameters()
{
}

bool PlannerParameters::readParameters(std::vector<std::pair<std::string, std::string> >& args)
{
    bool ret = true;
    ret &= readParameterMap(args);

    if(!known_plan_heuristic && !cyclic_cg_heuristic && !cyclic_cg_preferred_operators && !makespan_heuristic && !makespan_heuristic_preferred_operators && !no_heuristic && !rtpg_heuristic && !rtpg_heuristic_preferred_operators) {
        if(planMonitorFileName.empty()) {   // for monitoring this is irrelevant
            cerr << "Error: you must select at least one heuristic!" << endl;
            ret = false;
        }
    }
    if(timeout_if_plan_found < 0) {
        cerr << "Error: timeout_if_plan_found < 0, have: " << timeout_if_plan_found << endl;
        timeout_if_plan_found = 0;
        ret = false;
    }
    if(timeout_while_no_plan_found < 0) {
        cerr << "Error: timeout_while_no_plan_found < 0, have: " << timeout_while_no_plan_found << endl;
        timeout_while_no_plan_found = 0;
        ret = false;
    }

    return ret;
}

void PlannerParameters::dump() const
{
    cout << endl << "Planner Paramters:" << endl;
    cout << "Anytime Search: " << (anytime_search ? "Enabled" : "Disabled") << endl;
    cout << "Timeout if plan was found: " << timeout_if_plan_found << " seconds";
    if(timeout_if_plan_found == 0)
        cout << " (no timeout)";
    cout << endl;
    cout << "Timeout while no plan was found: " << timeout_while_no_plan_found << " seconds";
    if(timeout_while_no_plan_found == 0)
        cout << " (no timeout)";
    cout << endl;
    cout << "Greedy Search: " << (greedy ? "Enabled" : "Disabled") << endl;
    cout << "Verbose: " << (verbose ? "Enabled" : "Disabled") << endl;
    cout << "Lazy Heuristic Evaluation: " << (lazy_evaluation ? "Enabled" : "Disabled") << endl;

    cout << "Cyclic CG heuristic: " << (cyclic_cg_heuristic ? "Enabled" : "Disabled")
        << " \tPreferred Operators: " << (cyclic_cg_preferred_operators ? "Enabled" : "Disabled") << endl;
    cout << "Makespan heuristic: " << (makespan_heuristic ? "Enabled" : "Disabled")
        << " \tPreferred Operators: " << (makespan_heuristic_preferred_operators ? "Enabled" : "Disabled") << endl;
    cout << "No Heuristic: " << (no_heuristic ? "Enabled" : "Disabled") << endl;
    cout << "Cg Heuristic Zero Cost Waiting Transitions: "
        << (cg_heuristic_zero_cost_waiting_transitions ? "Enabled" : "Disabled") << endl;
    cout << "Cg Heuristic Fire Waiting Transitions Only If Local Problems Matches State: "
        << (cg_heuristic_fire_waiting_transitions_only_if_local_problems_matches_state ? "Enabled" : "Disabled") << endl;

    cout << "PrefOpsOrderedMode: " << (pref_ops_ordered_mode ? "Enabled" : "Disabled")
         << " with " << number_pref_ops_ordered_mode << " goals" << endl;
    cout << "PrefOpsCheapestMode: " << (pref_ops_cheapest_mode ? "Enabled" : "Disabled")
         << " with " << number_pref_ops_cheapest_mode << " goals" << endl;
    cout << "PrefOpsMostExpensiveMode: " << (pref_ops_most_expensive_mode ? "Enabled" : "Disabled")
         << " with " << number_pref_ops_most_expensive_mode << " goals" << endl;

    cout << "GValues by: ";
    switch(g_values) {
        case GMakespan:
            cout << "Makespan";
            break;
        case GCost:
            cout << "Cost";
            break;
    }
    cout << endl;

    cout << "Epsilonize internally: " << (epsilonize_internally ? "Enabled" : "Disabled") << endl;
    cout << "Epsilonize externally: " << (epsilonize_externally ? "Enabled" : "Disabled") << endl;

    cout << "Plan monitor file: \"" << planMonitorFileName << "\"";
    if(planMonitorFileName.empty()) {
        cout << " (no monitoring)";
    }
    cout << endl;

    cout << "Monitoring verify timestamps: " << (monitoring_verify_timestamps ? "Enabled" : "Disabled") << endl;

    cout << endl;
}

bool PlannerParameters::readParameterMap(std::vector<std::pair<std::string, std::string> >& args)
{
    for(const auto& arg : args) {
        const auto& key = arg.first;
        const auto& val = arg.second;
        if (key == "anytime") {
            anytime_search = true;
        } else if (key == "timeout_with_plan") {
            timeout_if_plan_found = atoi(val.c_str());
        } else if (key == "timeout_without_plan") {
            timeout_while_no_plan_found = atoi(val.c_str());
        } else if (key == "ordered_mode") {
            pref_ops_ordered_mode = true;
            number_pref_ops_ordered_mode = atoi(val.c_str());
        } else if (key == "cheapest_mode") {
            pref_ops_cheapest_mode = true;
            number_pref_ops_cheapest_mode = atoi(val.c_str());
        } else if (key == "expensive_mode") {
            pref_ops_most_expensive_mode = true;
            number_pref_ops_most_expensive_mode = atoi(val.c_str());
        } else if (key == "random_mode") {
            pref_ops_rand_mode = true;
            number_pref_ops_rand_mode = atoi(val.c_str());
        } else if (key == "greedy") {
            greedy = true;
        } else if (key == "lazy_evaluazion") {
            lazy_evaluation = false;
        } else if (key == "verbose") {
            verbose = false;
        } else if(key == "maximum cost") {
            max_cost = stod(val);
            assert(max_cost >= 0);
        } else if(key == "cost calculation") {
            if(val == "add")
                costCalculation = ADD;
            else if (val == "mul")
                costCalculation = MULTIPLY;
        } else if (key == "heuristic") {
            if(val == "cyclic_cg")
                cyclic_cg_heuristic = true;
            else if(val == "rtpg")
                rtpg_heuristic = true;
            else if(val == "rtpg_preferred")
                rtpg_heuristic_preferred_operators = true;
            else if(val == "cyclic_cg_preferred")
                cyclic_cg_preferred_operators = true;
            else if(val == "makespan")
                makespan_heuristic = true;
            else if(val == "makespan_preferred")
                makespan_heuristic_preferred_operators = true;
            else if(val == "no")
                no_heuristic = true;
            else if(val == "known_plan_heuristic")
                known_plan_heuristic = true;
        } else if (key == "g_value_evaluation") {
            if (val == "makespan") {
                g_values = GMakespan;
            } else if (val == "cost") {
                g_values = GCost;
            }
        } else if (key == "epsilonize_internally") {
            epsilonize_internally = true;
        } else if (key == "discretization") {
            discretizations = stoi(val);
            assert(discretizations > 0);
        } else if(key == "future_scheduling") {
            future_scheduling = true;
        } else if(key == "past_scheduling") {
            past_scheduling = true;
        } else if(key == "maximum makespan") {
            max_makespan = stod(val);
            assert(max_makespan >= 0.0);
        } else {
            cerr << "Unknown option: " << key << endl;
        }
    }

    return true;
}

