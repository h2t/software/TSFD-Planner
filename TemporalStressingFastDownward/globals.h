
#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <string>
#include <cmath>
#include <limits>

namespace tsfd {

#define EPSILON 0.00001 // for comparing doubles
#define EPS_TIME 0.01   // for separation of timepoints

}


#include "causal_graph.h"

namespace tsfd {
    class AxiomEvaluator;
    class CausalGraph;
    class DomainTransitionGraph;
    class Operator;
    class Axiom;
    class LogicAxiom;
    class NumericAxiom;
    class TimeStampedState;
    class SuccessorGenerator;

    enum OpenListMode {ALL=0, REGULAR=1, ORDERED=2, CHEAPEST=3, MOSTEXPENSIVE=4, RAND=5, CONCURRENT=6};

    struct PlanStep
    {
        double start_time;
        double duration;
        double cost;
        const Operator* op;
        const TimeStampedState* pred;   ///< Optional pointer to the predecessor state

        // create a useless PlanStep
        PlanStep() : start_time(-1), duration(0), cost(0), op(NULL), pred(NULL)
        {}
        PlanStep(double st, double dur, double c, const Operator* o, const TimeStampedState* p) :
                start_time(st), duration(dur), cost(c), op(o), pred(p)
        {}

        void dump() const;
    };

    class PlanStepCompareStartTime
    {
       public:
          bool operator()(const PlanStep & s1, const PlanStep & s2) const {
             return s1.start_time < s2.start_time;
          }
    };
    typedef std::vector<PlanStep> Plan;
    typedef std::vector<TimeStampedState*> PlanTrace;

    inline bool double_equals(double a, double b)
    {
        return std::abs(a - b) < EPSILON;
    }

    const double REALLYBIG = std::numeric_limits<double>::max();
    const double REALLYSMALL = -std::numeric_limits<double>::max();

    void read_variables(std::istream& in);
    void read_goal(std::istream& in);
    void dump_goal();
    void read_operators(std::istream& in);
    void read_logic_axioms(std::istream& in);
    void read_numeric_axioms(std::istream& in);
    void read_contains_universal_conditions(std::istream& in);
    void read_everything(std::istream& in);
    void evaluate_axioms_in_init();
    void reset_everything();
    void dump_everything();
    void dump_DTGs();

    void check_magic(std::istream &in, std::string magic);

    enum variable_type
    {
        logical,
        primitive_functional,
        subterm_functional,
        comparison
    };

    extern int g_last_arithmetic_axiom_layer;
    extern int g_comparison_axiom_layer;
    extern int g_first_logic_axiom_layer;
    extern int g_last_logic_axiom_layer;
    extern std::vector<std::string> g_variable_name;
    extern std::vector<int> g_variable_domain;
    extern std::vector<int> g_axiom_layers;
    extern std::vector<double> g_default_axiom_values;
    extern std::vector<variable_type> g_variable_types;
    extern TimeStampedState *g_initial_state;
    extern std::vector<std::pair<int, double> > g_goal;
    extern std::vector<Operator> g_operators;
    extern std::vector<Axiom*> g_axioms;
    extern AxiomEvaluator *g_axiom_evaluator;
    extern SuccessorGenerator *g_successor_generator;
    extern std::vector<DomainTransitionGraph *> g_transition_graphs;
    extern CausalGraph *g_causal_graph;

    class PlannerParameters;
    extern PlannerParameters g_parameters;

    inline bool is_functional(int var)
    {
        const variable_type& vt = g_variable_types[var];
        return (vt == primitive_functional || vt == subterm_functional);
    }

    extern Operator *g_let_time_pass;
    extern Operator *g_wait_operator;

    extern bool g_contains_universal_conditions;

    enum assignment_op
    {
        assign = 0, scale_up = 1, scale_down = 2, increase = 3, decrease = 4
    };
    enum binary_op
    {
        add = 0,
        subtract = 1,
        mult = 2,
        divis = 3,
        lt = 4,
        le = 5,
        eq = 6,
        ge = 7,
        gt = 8,
        ue = 9
    };
    enum trans_type
    {
        start = 0, end = 1, compressed = 2, ax = 3
    };
    enum condition_type
    {
        start_cond = 0, overall_cond = 1, end_cond = 2, ax_cond
    };

    std::istream& operator>>(std::istream &is, assignment_op &aop);
    std::ostream& operator<<(std::ostream &os, const assignment_op &aop);

    std::istream& operator>>(std::istream &is, binary_op &bop);
    std::ostream& operator<<(std::ostream &os, const binary_op &bop);

    std::istream& operator>>(std::istream &is, trans_type &tt);
    std::ostream& operator<<(std::ostream &os, const trans_type &tt);

    std::istream& operator>>(std::istream &is, condition_type &fop);
    std::ostream& operator<<(std::ostream &os, const condition_type &fop);

    void printSet(const std::set<int> s);
    double g_evaluate_expression(const std::string& expression_string, double a_val);
    double g_calculate_cost(const double current, const double action_cost);
}
