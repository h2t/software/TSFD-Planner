//
//  temporal_stressing_planner_controller.hpp
//  TemporalConcurrentPlanner
//
//  Created by Fabian Peller on 28.02.18.
//  Copyright © 2018 Fabian Peller. All rights reserved.
//

#pragma once

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <memory>
#include <string>

#include <cstdio>
#include <algorithm>

// Imports of planner.cc
#include "best_first_search.h"
#include "cyclic_cg_heuristic.h"
#include "no_heuristic.h"
#include "rtpg_heuristic.h"
#include "scrambled_egg_heuristic.h"

#include "globals.h"
#include "operator.h"
#include "partial_order_lifter.h"

#include "planner_parameters.h"

#include "pddl_to_sas_manager.h"
#include "time_stamped_state_change.h"

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include <cstdio>
#include <math.h>

#include <sys/times.h>
#include <sys/time.h>


namespace tsfd {
    class TemporalStressingPlannerController
    {
    public:
        TemporalStressingPlannerController();
        ~TemporalStressingPlannerController() {
            reset_everything();
        }

    private:
        double getCurrentTime();
        std::pair<double, double> update_plan(BestFirstSearchEngine&, double, double);

    public:
        void reset();
        std::string getPlannerProperty(const std::string&);
        bool setPlannerProperty(const std::string&, const std::string&);

        void clearDomain();
        void clearDomainProblems();

        bool loadDomain(const std::string&);
        bool loadDomainProblems(const std::string&);

        bool defineDomain(const std::string&);
        bool defineDomainProblems(const std::string&, const std::string&);

        bool setPlanProblem(const std::string&);

        void clearPlan();
        void resetPlan();
        bool buildPlan();

        Plan getPlan();
        TimeStampedState getPlanState();
        PlanStep getPlanAction();
        double getStartTimeOfOvernextPlanAction();

        bool isPlanDefined();
        bool isEndOfPlan();
        void advancePlan();
        void decrementPlan();

        bool checkPlanActionPreconditions(const TimeStampedState&);

        TimeStampedStateChange getPlanActionEffects(const TimeStampedState&);
        TimeStampedState getPlanActionEffectsState(const TimeStampedState&);
        TimeStampedState getPlanInitialState();
        TimeStampedState getDomainInitialState();
        TimeStampedStateChange getStateDifference(const TimeStampedState&, const TimeStampedState&);

        Operator getTemporalOperatorForName(const std::string&) const;

        std::vector<std::string> timeStampedStateToString(const TimeStampedState&);
        std::string stateAssignmentToString(const std::pair<int, double>& stateAssignment);
        std::vector<std::string> planToString(const Plan&);
        std::string planStepToString(const PlanStep&);

        SASAtom getSASForPDDLFact(const PDDLAtom& pddl) const;
        PDDLAtom getPDDLForSASVariable(const SASAtom& sas) const;

        int getStateSize() const;

    private:
        std::string domain;
        std::map<std::string, std::string> domainProblems;
        std::string useProblem;

        std::vector<std::pair<std::string, std::string> >plannerArguments;

        // set after plan was found
    public:
        Plan plan;
        double bestMakespan;
        double bestCost;

    private:
        // for observation of plan steps simulate plan
        size_t nextPlanStep;
        TimeStampedState currentState;
        PddlToSasManager pddl2sasManager;

    };
} // namespace tsfd

