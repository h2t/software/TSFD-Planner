#include "rtpg_heuristic.h"

#include "planner_parameters.h"
#include "globals.h"
#include "operator.h"
#include "state.h"

using namespace std;
using namespace tsfd;

void RTPGHeuristic::initialize()
{
    cout << "Initializing rtpg heuristic..." << endl;
}

bool RTPGHeuristic::is_applicable(const std::vector<std::vector<double> >& s, const std::vector<std::vector<std::pair<double, double> > >& ns, const Prevail& prevail) const
{
    return is_applicable_at(s, ns, prevail) != -1;
}

bool RTPGHeuristic::is_applicable(const std::vector<std::vector<double> >& s, const std::vector<std::vector<std::pair<double, double> > >& ns, const PrePost& prepost) const
{
    return is_applicable_at(s, ns, prepost) != -1;
}
double RTPGHeuristic::is_applicable_at(const std::vector<std::vector<double> >& current_state_set_at, const std::vector<std::vector<std::pair<double, double> > >& current_state_numeric_set_at, const Prevail& prevail) const {
    int var = prevail.var;
    double prev = prevail.prev;

    if(g_variable_types[var] == variable_type::logical) {
        //cout << "check " << var << " = " << prev << " is " << current_state_set_at[var][prev] << endl;
        return current_state_set_at[var][prev];
    }

    // numeric var
    auto it = std::find_if(current_state_numeric_set_at[var].begin(), current_state_numeric_set_at[var].end(), [&prev](const std::pair<double, double>& p) {
        return p.first == prev;
    });

    if(it == current_state_numeric_set_at[var].end()) {
        return -1;
    }

    return it->second;
}
double RTPGHeuristic::is_applicable_at(const std::vector<std::vector<double> >& current_state_set_at, const std::vector<std::vector<std::pair<double, double> > >& current_state_numeric_set_at, const PrePost& prepost) const {
    int var = prepost.var;
    double pre = prepost.pre;

    if(pre < 0) {
        // now applicable because value doesn't matter
        return 0.0;
    }

    assert(var >= 0);
    assert(pre >= 0);

    if(g_variable_types[var] == variable_type::logical) {
        //cout << "check " << var << " = " << pre << " is " << current_state_set_at[var][pre] << endl;
        return current_state_set_at[var][pre];
    }

    // numeric var TODO mustnot be earliest
    auto it = std::find_if(current_state_numeric_set_at[var].begin(), current_state_numeric_set_at[var].end(), [&pre](const std::pair<double, double>& p) {
        return p.first == pre;
    });

    if(it == current_state_numeric_set_at[var].end()) {
        //cout << "Cant find value " << pre << " for var " << var << "in vector" << endl;
        //for(const auto& p : state[var]) {
        //    cout << "(" << p.first << ", " << p.second << ")" << endl;
        //}
        return -1;
    }

    return it->second;
}

double RTPGHeuristic::get_startvalue_of_op(const std::vector<std::vector<double> >& current_state_set_at, const std::vector<std::vector<std::pair<double, double> > >& current_state_numeric_set_at, const Operator& op) const
{
    double max_value_of_start_conds = 0.0;

    for(const auto& prevail : op.get_prevail_start()) {
        double starting = this->is_applicable_at(current_state_set_at, current_state_numeric_set_at, prevail);

        if(starting == -1)
            return -1;

        max_value_of_start_conds = std::max(max_value_of_start_conds, starting);
    }

    for(const auto& prepost : op.get_pre_post_start()) {
        double starting = this->is_applicable_at(current_state_set_at, current_state_numeric_set_at, prepost);

        if(starting == -1)
            return -1;

        max_value_of_start_conds = std::max(max_value_of_start_conds, starting);
    }

    return max_value_of_start_conds;
}

bool RTPGHeuristic::is_applicable(const std::vector<std::vector<double> >& current_state_set_at, const std::vector<std::vector<std::pair<double, double> > >& current_state_numeric_set_at, const Operator& op) const
{
    // Lazy: We don't have a real time stamped state here, so we use the initial duration
    //double duration = op.initial_duration;
    double max_timestamp_of_start_conds = this->get_startvalue_of_op(current_state_set_at, current_state_numeric_set_at, op);

    if(max_timestamp_of_start_conds == -1) {
        //cout << " > TS is -1" << endl;
        return false;
    }

    for(const auto& prevail : op.get_prevail_overall()) {
        if(!this->is_applicable(current_state_set_at, current_state_numeric_set_at, prevail)) {
            //cout << "Not available overall" << endl;
            return false;
        }
    }

    // Create copy of state with added prepost start
    //cout << "copy state" << endl;
    /*std::vector<std::vector<double> > copied_state = current_state_set_at;
    std::vector<std::vector<std::pair<double, double> > > copied_numeric_state = current_state_numeric_set_at;
    std::vector<std::vector<const Operator*> > set_by = this->set_by; // copy dummy
    this->apply_effects_to_state(copied_state, copied_numeric_state, set_by, op.get_pre_post_start(), op, max_timestamp_of_start_conds);

    //cout << "Check Prevail end" << endl;
    for(const auto& prevail : op.get_prevail_end()) {
        double available_at = this->is_applicable_at(copied_state, copied_numeric_state, prevail);
        if(available_at > max_timestamp_of_start_conds + duration || available_at == -1) {
            //cout << "Not available at end" << endl;
            return false;
        }
    }

    //cout << "Check prepost end" << endl;
    for(const auto& prepost : op.get_pre_post_end()) {
        double available_at = this->is_applicable_at(copied_state, copied_numeric_state, prepost);
        if(available_at > max_timestamp_of_start_conds + duration || available_at == -1) {
            //cout << "Not available at end post" << endl;
            return false;
        }
    }*/

    return true;
}

bool RTPGHeuristic::apply_effects_to_state(std::vector<std::vector<double> >& state, std::vector<std::vector<std::pair<double, double> > >& numeric_state, std::vector<std::vector<const Operator*> >& set_by, const std::vector<PrePost>& preposts, const Operator& op, double val) const
{
    bool something_applied = false;
    for(const auto& prepost : preposts) {
        int var = prepost.var_post;
        double post = prepost.post;

        if(var == -1) {
            //cout << "Variable of prepost (" << prepost.var << " = " <<  prepost.pre << " => " << prepost.var_post << " = " << prepost.post << ") is -1" << endl;
            var = prepost.var;
        }

        assert(var >= 0);
        assert(post >= 0);

        if(g_variable_types[var] == variable_type::logical) {
            //cout << "Ass " << var << " = " << post << " has ts " << state[var][post] << endl;
            if(state[var][post] == -1 || state[var][post] > val) {
                //cout << "setting op " << &op << " and val" << endl;
                state[var][post] = val;
                set_by[var][post] = &op;
                something_applied = true;
            }

            continue;
        }

        // numeric
        const auto effect = std::make_pair(post, val);

        if(std::find(numeric_state[var].begin(), numeric_state[var].end(), effect) == numeric_state[var].end()) {
            numeric_state[var].push_back(effect);
            something_applied = true;
        }
    }
    return something_applied;
}

double RTPGHeuristic::compute_heuristic(const TimeStampedState &state)
{
    //cout << "Got state" << endl;
    //state.dump(true);

    // cout << "Evaluate state" << endl;

    if(state.satisfies(g_goal) && state.scheduled_effects.empty())
        return 0.0;

    double heuristic = DEAD_END;

    HeuristicValueCache::const_iterator it = cacheHeuristicValue.find(state);
    if(it != cacheHeuristicValue.end()) {
        num_cache_hits++;
        heuristic = cacheHeuristicValue[state];
        reset_pref_ops_from_cache(heuristic, state);

    } else {
        if(!compute_goal_fulfilling_assignment(state)) {
            heuristic = DEAD_END;
        } else {
            //cout << "Found goal assignment, get h" << endl;
            heuristic = compute_heuristic_value_of_relaxed(state);
        };
        compute_pref_ops(heuristic, state);
        cacheHeuristicValue[state] = heuristic;
        cachePrefOps[state] = prefOpsSortedByCorrespondigGoal;

    }

    //cout << "Found heuristic " << heuristic << endl;
    return heuristic;
}

bool RTPGHeuristic::goal_reached() const
{
    for(const auto& goal_assignment : g_goal) {
        int var = goal_assignment.first;
        double val = goal_assignment.second;

        if(g_variable_types[var] == variable_type::logical) {
            if(current_state_set_at[var][val] == -1) {
                return false;
            }
            continue;
        }

        // numeric
        auto it = std::find_if(current_state_numeric_set_at[var].begin(), current_state_numeric_set_at[var].end(), [&val](const std::pair<double, double>& p) {
            return p.first == val;
        });

        if(it == current_state_numeric_set_at[var].end()) {
            return false;
        }
    }
    return true;
}

bool RTPGHeuristic::compute_goal_fulfilling_assignment(const TimeStampedState &state)
{
    std::vector<std::vector<std::pair<double, double> > > last_state_numeric_set_at;
    std::vector<std::vector<double> > last_state_set_at;

    current_state_numeric_set_at.clear();
    current_state_set_at.clear();
    set_by.clear();
    this->costsOfGoals.clear();
    for(unsigned int i = 0; i < g_goal.size(); ++i) {
        this->costsOfGoals.push_back(-1);
    }

    for(unsigned int i = 0; i < g_variable_domain.size(); i++) {
        current_state_numeric_set_at.push_back(std::vector<std::pair<double, double> >());
        current_state_set_at.push_back(std::vector<double>());
        set_by.push_back(std::vector<const Operator*>());

        for(int j = 0; j < g_variable_domain[i]; ++j) {
            set_by[i].push_back(NULL);
            current_state_set_at[i].push_back(-1);
        }
    }

    double initial_state_val = (g_parameters.g_values == PlannerParameters::GMakespan) ? state.timestamp : state.cost;

    for(unsigned int i = 0; i < state.state.size(); ++i) {
        if(g_variable_types[i] == variable_type::logical) {
            current_state_set_at[i][state[i]] = initial_state_val;
        } else {
            current_state_numeric_set_at[i].push_back(std::make_pair(state[i], initial_state_val));
        }
    }

    for(const auto& prepost : state.scheduled_effects) {
        int var  = prepost.var_post;
        int post = prepost.post;

        if(var == -1) {
            var = prepost.var;
        }

        assert(var >= 0);
        assert(post >= 0);

        double val_at_effect_finished = (g_parameters.g_values == PlannerParameters::GMakespan) ? state.timestamp + prepost.time_increment : state.cost;

        if(g_variable_types[var] == variable_type::logical) {
            if(current_state_set_at[var][post] == -1 || current_state_set_at[var][post] > val_at_effect_finished) {
                current_state_set_at[var][post] = val_at_effect_finished;
                set_by[var][post] = prepost.set_by_operator;
            }
        } else {
            auto it = std::find_if(current_state_numeric_set_at[var].begin(), current_state_numeric_set_at[var].end(), [&post](const std::pair<double, double>& p) {
                return p.first == post;
            });

            if(it == current_state_numeric_set_at[var].end()) {
                current_state_numeric_set_at[var].push_back(std::make_pair(post, val_at_effect_finished));

            }
        }
    }

    bool goal_reached = this->goal_reached();
    while(!goal_reached) {

        last_state_set_at = current_state_set_at;
        last_state_numeric_set_at = current_state_numeric_set_at;

        bool something_applied = false;
        for(const auto& op : g_operators) {

            //cout << "Check operator " << op.get_name() << endl;

            // check if operator is applicable
            if(!this->is_applicable(last_state_set_at, last_state_numeric_set_at, op)) {
                //cout << " > Operator " << op.get_name() << " is not applicable" << endl;
                continue;
            }

            // apply effects to current state
            double duration = op.initial_duration;
            double cost = op.initial_cost;
            double max_value_of_start_conds = this->get_startvalue_of_op(last_state_set_at, last_state_numeric_set_at, op);

            assert(max_value_of_start_conds != -1);

            double new_cost =(g_parameters.costCalculation == PlannerParameters::ADD) ? max_value_of_start_conds + cost : 1 - (1 - max_value_of_start_conds) * (1 - cost);

            //cout << "Operator " << op.get_name() << " is applicable" << endl;

            double value_before_execution = (g_parameters.g_values == PlannerParameters::GMakespan) ? max_value_of_start_conds : new_cost;
            double value_after_execution = (g_parameters.g_values == PlannerParameters::GMakespan) ? max_value_of_start_conds + duration : new_cost;

            if(this->apply_effects_to_state(current_state_set_at, current_state_numeric_set_at, set_by, op.get_pre_post_start(), op, value_before_execution)) {
                something_applied = true;
            }

            if(this->apply_effects_to_state(current_state_set_at, current_state_numeric_set_at, set_by, op.get_pre_post_end(), op, value_after_execution)) {
                something_applied = true;
            }
        }

        if(!something_applied) {
            //cout << "Return heuristic DEAD_END" << endl;
            return false;
        }

        goal_reached = this->goal_reached();
    }
    return true;
}

vector<const Operator* > RTPGHeuristic::get_all_operators_that_causes_assignment(const std::pair<int, double>& ass, vector<const Operator*>& already_checked) const
{
    //cout << "check " << ass.first << " = " << ass.second << endl;

    vector<const Operator*> useful_ops;

    assert(ass.first >= 0);

    if(ass.second < 0) {
        //cout << "ass.second < 0" << endl;
        return useful_ops;
    }

    if(g_variable_types[ass.first] != variable_type::logical) {
        //cout << "Numeric prev" << endl;
        return useful_ops; // cant currently handle (no set by op)
    }

    if(current_state_set_at[ass.first][ass.second] < 0) {
        // found an scheduled operator in initial state. Its preconds may not be valid at relaxed goal
        //cout << "Scheduled prev" << endl;
        return useful_ops;
    }

    const Operator* set_through = this->set_by[ass.first][ass.second];

    if(set_through == NULL) {
        //cout << "Set through is null" << endl;
        return useful_ops;
    }

    if(std::find(already_checked.begin(), already_checked.end(), set_through) != already_checked.end()) {
        //cout << "OP " << set_through->get_name() << " already inserted" << endl;
        return useful_ops;
    }

    already_checked.push_back(set_through);

    // We need this operator
    useful_ops.push_back(set_through);

    //cout << "Search for other prevs of " << set_through->get_name() << endl;
    for(const auto& ass : set_through->get_prevail_start()) {
        std::pair<int, double> assignment(ass.var, ass.prev);
        vector<const Operator*> first = this->get_all_operators_that_causes_assignment(assignment, already_checked);
        useful_ops.insert(useful_ops.end(), first.begin(), first.end());
    }
    for(const auto& ass : set_through->get_pre_post_start()) {
        std::pair<int, double> assignment(ass.var, ass.pre);
        vector<const Operator*> first = this->get_all_operators_that_causes_assignment(assignment, already_checked);
        useful_ops.insert(useful_ops.end(), first.begin(), first.end());
    }
    for(const auto& ass : set_through->get_prevail_overall()) {
        std::pair<int, double> assignment(ass.var, ass.prev);
        vector<const Operator*> first = this->get_all_operators_that_causes_assignment(assignment, already_checked);
        useful_ops.insert(useful_ops.end(), first.begin(), first.end());
    }
    for(const auto& ass : set_through->get_prevail_end()) {
        std::pair<int, double> assignment(ass.var, ass.prev);
        vector<const Operator*> first = this->get_all_operators_that_causes_assignment(assignment, already_checked);
        useful_ops.insert(useful_ops.end(), first.begin(), first.end());
    }
    for(const auto& ass : set_through->get_pre_post_end()) {
        std::pair<int, double> assignment(ass.var, ass.pre);
        vector<const Operator*> first = this->get_all_operators_that_causes_assignment(assignment, already_checked);
        useful_ops.insert(useful_ops.end(), first.begin(), first.end());
    }

    //cout << "Return useful ops" << endl;
    return useful_ops;
}


double RTPGHeuristic::compute_heuristic_value_of_relaxed(const TimeStampedState& state)
{
    //cout << "Clear prefOps" << endl;
    for(unsigned int i = 0; i < prefOpsSortedByCorrespondigGoal.size(); ++i) {
        prefOpsSortedByCorrespondigGoal[i].clear();
    }

    double max = 0.0;
    double sum = 0.0;

    for(unsigned int i = 0; i < g_goal.size(); ++i)
    {
        const auto& goal_assignment = g_goal[i];

        int var = goal_assignment.first;
        double val = goal_assignment.second;

        //cout << "Check goal " << var << " = " << val << endl;

        double best_value = HUGE_VAL;
        if(g_variable_types[var] == variable_type::logical) {
            best_value = current_state_set_at[var][val];
        } else {

            // numeric goal
            for(const auto& pair : current_state_numeric_set_at[var]) {
                if(pair.first == val) {
                    best_value = std::min(best_value, pair.second);
                }
            }
        }

        assert(best_value >= 0);
        assert(best_value != HUGE_VAL);

        this->costsOfGoals[i] = best_value;

        //cout << "Get prefOps" << endl;
        this->prefOpsSortedByCorrespondigGoal.push_back(std::set<const Operator*>());
        std::pair<int, double> pair(var, val);
        std::vector<const Operator*> checked;
        std::vector<const Operator*> checked_sum;

        for(const auto& scheduled : state.operators) { // TODO ändere zu referenzen
            for(const auto& op : g_operators) {
                if(op.get_name() == scheduled.get_name() && op.used_acceleration == scheduled.used_acceleration) {
                    checked.push_back(&op);
                    checked_sum.push_back(&op);
                    break;
                }
            }
        }

        std::vector<const Operator*> responsible_ops = this->get_all_operators_that_causes_assignment(pair, checked);

        for(const auto& op : responsible_ops) {
            bool applicable = true;
            double duration = op->get_duration(&state);
            double cost = op->get_cost(&state);

            double new_cost =(g_parameters.costCalculation == PlannerParameters::ADD) ? state.cost + cost : 1 - (1 - state.cost) * (1 - cost);

            double stateValueBeforeExecution = (g_parameters.g_values == PlannerParameters::GMakespan) ? state.timestamp : new_cost;
            double stateValueAfterExecution = (g_parameters.g_values == PlannerParameters::GMakespan) ? state.timestamp + duration : state.cost + cost; new_cost;

            if(std::find(checked_sum.begin(), checked_sum.end(), op) == checked_sum.end()) {
                // Do not count operators twice
                sum += (g_parameters.g_values == PlannerParameters::GMakespan) ? duration : cost;
                checked_sum.push_back(op);
            }

            for(const auto& prevail : op->get_prevail_start()) {
                int var = prevail.var;
                int prev = prevail.prev;
                if(current_state_set_at[var][prev] > stateValueBeforeExecution) {
                    applicable = false;
                    break;
                }
            }
            if(!applicable)
                continue;
            for(const auto& prepost : op->get_pre_post_start()) {
                int var = prepost.var;
                int pre = prepost.pre;

                if(pre < 0) {
                    continue;
                }

                if(current_state_set_at[var][pre] > stateValueBeforeExecution) {
                    applicable = false;
                    break;
                }
            }
            if(!applicable)
                continue;
            for(const auto& prevail : op->get_prevail_overall()) {
                int var = prevail.var;
                int prev = prevail.prev;
                if(current_state_set_at[var][prev] > stateValueBeforeExecution) {
                    applicable = false;
                    break;
                }
            }
            if(!applicable)
                continue;
            for(const auto& prevail : op->get_prevail_end()) {
                int var = prevail.var;
                int prev = prevail.prev;
                if(current_state_set_at[var][prev] > stateValueAfterExecution) {
                    applicable = false;
                    break;
                }
            }
            if(!applicable)
                continue;
            for(const auto& prepost : op->get_pre_post_end()) {
                int var = prepost.var;
                int pre = prepost.pre;

                if(pre < 0) {
                    continue;
                }

                if(current_state_set_at[var][pre] > stateValueAfterExecution) {
                    applicable = false;
                    break;
                }
            }
            if(!applicable)
                continue;

            this->prefOpsSortedByCorrespondigGoal[i].insert(op);
        }

        //cout << "Calculate max" << endl;
        max = std::max(max, best_value);
        sum = sum + best_value;
    }

    //cout << "Return heuristic " << max << endl;
    return max;
}

void RTPGHeuristic::compute_pref_ops(double heuristic, const TimeStampedState &state) {
    if(heuristic != DEAD_END && heuristic != 0) {
        if(g_parameters.pref_ops_cheapest_mode || g_parameters.pref_ops_most_expensive_mode ||
                g_parameters.pref_ops_ordered_mode || g_parameters.pref_ops_rand_mode) {
            assert(g_parameters.number_pref_ops_cheapest_mode +
                    g_parameters.number_pref_ops_most_expensive_mode +
                    g_parameters.number_pref_ops_ordered_mode +
                    g_parameters.number_pref_ops_rand_mode > 0);
        }
        cacheCostsToGoal[state] = costsOfGoals;
        set_specific_pref_ops(state);
    }
}

void RTPGHeuristic::reset_pref_ops_from_cache(double heuristic, const TimeStampedState &state) {
    if(heuristic != DEAD_END && heuristic != 0) {
        PrefOpsCache::const_iterator it = cachePrefOps.find(state);
        assert(it != cachePrefOps.end());
        prefOpsSortedByCorrespondigGoal = it->second;

        CostsToGoalCache::const_iterator it2 = cacheCostsToGoal.find(state);
        assert(it2 != cacheCostsToGoal.end());
        costsOfGoals = it2->second;

        set_specific_pref_ops(state);
    }
}

void RTPGHeuristic::set_specific_pref_ops(const TimeStampedState &state) {
    vector<double> temp_costsOfGoals = costsOfGoals;
    if(g_parameters.pref_ops_ordered_mode) {
        setFirstPrefOpsExternally(g_parameters.number_pref_ops_ordered_mode);
    }
    if(g_parameters.pref_ops_cheapest_mode) {
        setCheapestPrefOpsExternally(g_parameters.number_pref_ops_cheapest_mode);
    }
    costsOfGoals = temp_costsOfGoals;
    if(g_parameters.pref_ops_most_expensive_mode) {
        setMostExpensivePrefOpsExternally(g_parameters.number_pref_ops_most_expensive_mode);
    }
    costsOfGoals = temp_costsOfGoals;
    if(g_parameters.pref_ops_rand_mode) {
        setRandPrefOpsExternally(g_parameters.number_pref_ops_rand_mode);
    }
    if(g_parameters.pref_ops_concurrent_mode) {
        setConcurrentPrefOpsExternally(state);
    }
    setAllPrefOpsExternally();
}

void RTPGHeuristic::setCheapestPrefOpsExternally(unsigned int number) {
    cheapestGoals.clear();
    for(unsigned int i = 0; i < number; ++i) {
        double cheapestGoal = numeric_limits<double>::max();
        int idx = -1;
        for(unsigned int j = 0; j < costsOfGoals.size(); ++j) {
            assert(double_equals(costsOfGoals[j],-1.0) || costsOfGoals[j] >= 0.0);
            if(!double_equals(costsOfGoals[j],-1.0) && (costsOfGoals[j] < cheapestGoal)) {
                cheapestGoal = costsOfGoals[j];
                idx = j;
            }
        }
        if(idx == -1) {
            return;
        }
        set<const Operator*> prefOps = prefOpsSortedByCorrespondigGoal[idx];
        for(set<const Operator*>::iterator it = prefOps.begin(); it != prefOps.end(); ++it) {
            set_preferred(*it, CHEAPEST);
        }
        costsOfGoals[idx] = -1.0;
    }
}

void RTPGHeuristic::setMostExpensivePrefOpsExternally(unsigned int number) {
    mostExpensiveGoals.clear();
    for(unsigned int i = 0; i < number; ++i) {
        double mostExpensiveGoal = -numeric_limits<double>::max();
        int idx = -1;
        for(unsigned int j = 0; j < costsOfGoals.size(); ++j) {
            assert(double_equals(costsOfGoals[j],-1.0) || costsOfGoals[j] >= 0.0);
            if(!double_equals(costsOfGoals[j],-1.0) && (costsOfGoals[j] > mostExpensiveGoal)) {
                mostExpensiveGoal = costsOfGoals[j];
                idx = j;
            }
        }
        if(idx == -1) {
            return;
        }
        set<const Operator*> prefOps = prefOpsSortedByCorrespondigGoal[idx];
        for(set<const Operator*>::iterator it = prefOps.begin(); it != prefOps.end(); ++it) {
            set_preferred(*it, MOSTEXPENSIVE);
        }
        costsOfGoals[idx] = -1.0;
    }
}

void RTPGHeuristic::setRandPrefOpsExternally(unsigned int number) {
    randGoals.clear();
    vector<int> notYetSatiesfiedGoals;
    for(unsigned int j = 0; j < costsOfGoals.size(); ++j) {
        assert(double_equals(costsOfGoals[j],-1.0) || costsOfGoals[j] >= 0.0);
        if(!double_equals(costsOfGoals[j],-1.0)) {
            notYetSatiesfiedGoals.push_back(j);
        }
    }
    for(unsigned int i = 0; i < number; ++i) {
        if(notYetSatiesfiedGoals.size() == 0) {
            return;
        }
        int idx = rand() % notYetSatiesfiedGoals.size();
        set<const Operator*> prefOps = prefOpsSortedByCorrespondigGoal[notYetSatiesfiedGoals[idx]];
        for(set<const Operator*>::iterator it = prefOps.begin(); it != prefOps.end(); ++it) {
            set_preferred(*it, RAND);
        }
        notYetSatiesfiedGoals[idx] = notYetSatiesfiedGoals[notYetSatiesfiedGoals.size()-1];
        notYetSatiesfiedGoals.pop_back();
    }
}

void RTPGHeuristic::setConcurrentPrefOpsExternally(const TimeStampedState &state) {
    for(unsigned int i = 0; i < prefOpsSortedByCorrespondigGoal.size(); ++i) {
        set<const Operator*>& prefOps = prefOpsSortedByCorrespondigGoal[i];
        for(set<const Operator*>::iterator it = prefOps.begin(); it != prefOps.end(); ++it) {
            bool isMutex = false;
            for(unsigned int i = 0; i < preferred_operators_concurrent.size(); i++) {
                const Operator* prefOp = preferred_operators_concurrent[i];
                if((*it)->isDisabledBy(prefOp) || prefOp->isDisabledBy(*it)) {
                    isMutex = true;
                    break;
                }
            }
            if(!isMutex && (*it)->is_applicable(state)) {
                set_preferred(*it, CONCURRENT);
            }
        }
    }
}

void RTPGHeuristic::setFirstPrefOpsExternally(unsigned int number) {
    for(unsigned int i = 0; i < prefOpsSortedByCorrespondigGoal.size(); ++i) {
        if(number <= 0) {
            return;
        }
        if(!double_equals(costsOfGoals[i],-1.0)) {
            assert(i < prefOpsSortedByCorrespondigGoal.size());
            set<const Operator*>& prefOpsSortedOfThisGoal = prefOpsSortedByCorrespondigGoal[i];
            for(set<const Operator*>::iterator it = prefOpsSortedOfThisGoal.begin(); it != prefOpsSortedOfThisGoal.end(); ++it) {
                set_preferred(*it, ORDERED);
            }
            number--;
        }
    }
}

void RTPGHeuristic::setAllPrefOpsExternally() {
    for(unsigned int i = 0; i < prefOpsSortedByCorrespondigGoal.size(); ++i) {
        set<const Operator*>& prefOps = prefOpsSortedByCorrespondigGoal[i];
        for(set<const Operator*>::iterator it = prefOps.begin(); it != prefOps.end(); ++it) {
            set_preferred(*it, REGULAR);
        }
    }
}
