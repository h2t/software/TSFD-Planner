#include "scrambled_egg_heuristic.h"

#include "globals.h"
#include "operator.h"
#include "state.h"

using namespace std;
using namespace tsfd;

void ScrambledEggHeuristic::initialize()
{
    cout << "Initializing scrambled egg heuristic..." << endl;

    std::vector<std::string> optimal_plan;

    optimal_plan.push_back("humanPrepareIngredients human__2 eggs__01 egg_dough__01 roundtable2__f4ea");
    optimal_plan.push_back("humanPrepareStove human__2 stove__6 pan__01 stove__6227");
    optimal_plan.push_back("grasp testAgent__1 handleft3a__52 roundtable2__f4ea oil__01");
    optimal_plan.push_back("move testAgent__1 roundtable2__f4ea fridge__6228");
    optimal_plan.push_back("open testAgent__1 handright3a__51 fridge__6228");
    optimal_plan.push_back("move testAgent__1 fridge__6228 stove__6227");
    optimal_plan.push_back("pour testAgent__1 handleft3a__52 stove__6227 pan__01 oil__01");
    optimal_plan.push_back("move testAgent__1 stove__6227 roundtable2__f4ea");
    optimal_plan.push_back("putdown testAgent__1 handleft3a__52 roundtable2__f4ea oil__01");
    optimal_plan.push_back("move testAgent__1 roundtable2__f4ea fridge__6228");
    optimal_plan.push_back("graspWithDoors testAgent__1 handright3a__51 fridge__6228 milk__01");
    optimal_plan.push_back("move testAgent__1 fridge__6228 roundtable2__f4ea");
    optimal_plan.push_back("pour testAgent__1 handright3a__51 roundtable2__f4ea egg_dough__01 milk__01");
    optimal_plan.push_back("putdown testAgent__1 handright3a__51 roundtable2__f4ea milk__01");
    optimal_plan.push_back("grasp testAgent__1 handright3a__51 roundtable2__f4ea egg_dough__01");
    optimal_plan.push_back("shake testAgent__1 handright3a__51 egg_dough__01 eggs__01 milk__01");
    optimal_plan.push_back("grasp testAgent__1 handleft3a__52 roundtable2__f4ea stirrer__32");
    optimal_plan.push_back("move testAgent__1 roundtable2__f4ea stove__6227");
    optimal_plan.push_back("pour testAgent__1 handright3a__51 stove__6227 pan__01 egg_dough__01");
    optimal_plan.push_back("stirAndCook testAgent__1 stove__6 handleft3a__52 stirrer__32 stove__6227 egg_dough__01 pan__01 oil__01");
    optimal_plan.push_back("move testAgent__1 stove__6227 roundtable2__f4ea");
    optimal_plan.push_back("putdown testAgent__1 handright3a__51 roundtable2__f4ea egg_dough__01");
    optimal_plan.push_back("move testAgent__1 roundtable2__f4ea fridge__6228");
    optimal_plan.push_back("close testAgent__1 handright3a__51 fridge__6228");

    /*
    optimal_plan.push_back("humanPrepareIngredients human__2 eggs__01 egg_dough__01 roundtable2__f4ea");
    optimal_plan.push_back("humanPrepareStove human__2 stove__6 pan__01 stove__6227");
    optimal_plan.push_back("move testAgent__1 roundtable2__f4ea fridge__6228");
    optimal_plan.push_back("open testAgent__1 handright3a__51 fridge__6228");
    optimal_plan.push_back("graspWithDoors testAgent__1 handright3a__51 fridge__6228 milk__01");
    optimal_plan.push_back("move testAgent__1 fridge__6228 roundtable2__f4ea");
    optimal_plan.push_back("pour testAgent__1 handright3a__51 roundtable2__f4ea egg_dough__01 milk__01");
    optimal_plan.push_back("grasp testAgent__1 handleft3a__52 roundtable2__f4ea oil__01");
    optimal_plan.push_back("putdown testAgent__1 handright3a__51 roundtable2__f4ea milk__01");
    optimal_plan.push_back("move testAgent__1 roundtable2__f4ea stove__6227");
    optimal_plan.push_back("pour testAgent__1 handleft3a__52 stove__6227 pan__01 oil__01");
    optimal_plan.push_back("move testAgent__1 stove__6227 fridge__6228");
    optimal_plan.push_back("close testAgent__1 handright3a__51 fridge__6228");
    optimal_plan.push_back("move testAgent__1 fridge__6228 roundtable2__f4ea");
    optimal_plan.push_back("grasp testAgent__1 handright3a__51 roundtable2__f4ea egg_dough__01");
    optimal_plan.push_back("shake testAgent__1 handright3a__51 egg_dough__01 eggs__01 milk__01");
    optimal_plan.push_back("putdown testAgent__1 handleft3a__52 roundtable2__f4ea oil__01");
    optimal_plan.push_back("grasp testAgent__1 handleft3a__52 roundtable2__f4ea stirrer__32");
    optimal_plan.push_back("move testAgent__1 roundtable2__f4ea stove__6227");
    optimal_plan.push_back("pour testAgent__1 handright3a__51 stove__6227 pan__01 egg_dough__01");
    optimal_plan.push_back("stirAndCook testAgent__1 stove__6 handleft3a__52 stirrer__32 stove__6227 egg_dough__01 pan__01 oil__01");
    */

    TimeStampedState last_state = TimeStampedState(*g_initial_state);
    this->plan_states.push_back(last_state);

    for(const auto& op_name : optimal_plan) {
        for(const auto& op : g_operators) {
            if(op.get_initial_name() == op_name) {

                assert(op.is_applicable(last_state));
                // schedule next op
                TimeStampedState next_state = TimeStampedState(last_state, op);
                this->plan_states.push_back(next_state); // also add at-start effects

                // execute op
                next_state = next_state.let_time_pass(next_state.next_happening() - next_state.timestamp, true);
                last_state = next_state;

                this->plan_states.push_back(next_state); // also add at end effects
                break;
            }
        }
    }

    cout << "Length of plan " << this->plan_states.size() << endl;
}

double ScrambledEggHeuristic::compute_heuristic(const TimeStampedState &state)
{

    if(state.satisfies(g_goal) && state.scheduled_effects.empty())
        return 0.0;

    TimeStampedState executed = state;
    while(executed.operators.size() > 0) {
        executed = executed.let_time_pass(executed.next_happening() - executed.timestamp, true);
    }

    unsigned int plan_step_index = 0;
    for(const auto& plan_state : this->plan_states) {
        plan_step_index++;

        assert(plan_state.state.size() == executed.state.size());
        bool all_equal = true;
        for(unsigned int i = 0; i < executed.state.size(); ++i) {
            if(plan_state[i] != executed[i]) {
                all_equal = false;
                break;
            }
        }

        if(all_equal) {
            //cout << "RETURN HEURISTIC " << endl;
            return plan_states.size() - plan_step_index;
        }
    }

    //cout << "RETURN DEAD END" << endl;
    return DEAD_END;
}
