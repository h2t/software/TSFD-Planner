#include "globals.h"

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cassert>
#include <cmath>
using namespace std;
using namespace tsfd;

#include "axioms.h"
//#include "causal_graph.h"
#include "domain_transition_graph.h"
#include "operator.h"
#include "state.h"
#include "successor_generator.h"
#include "planner_parameters.h"
#include "pddl_to_sas_manager.h"

#include "exprtk.h"


void PlanStep::dump() const
{
    cout << start_time << ": " << op->get_name() << "[" << duration << "]"
        << endl;
}

void tsfd::check_magic(istream &in, string magic)
{
    string word;
    in >> word;
    if(word != magic) {
        cout << "Failed to match magic word '" << magic << "'." << endl;
        cout << "Got '" << word << "'." << endl;
        exit(1);
    }
}

void tsfd::read_variables(istream &in)
{
    check_magic(in, "begin_variables");
    int count;
    in >> count;
    for(int i = 0; i < count; i++) {
        string name;
        in >> name;
        g_variable_name.push_back(name);
        int range;
        in >> range;
        g_variable_domain.push_back(range);
        int layer;
        in >> layer;
        g_axiom_layers.push_back(layer);
        //identify variable type
        if(range != -1) {
            g_variable_types.push_back(logical);
            //changes to comparison if a comparison axiom is detected
        } else {
            g_variable_types.push_back(primitive_functional);
            //changes to subterm_functional if a numeric axiom is detected
        }
    }
    check_magic(in, "end_variables");
}

void tsfd::read_goal(istream &in)
{
    check_magic(in, "begin_goal");
    int count;
    in >> count;
    for(int i = 0; i < count; i++) {
        int var;
        double val;
        in >> var >> val;
        g_goal.push_back(make_pair(var, val));
    }
    check_magic(in, "end_goal");
}

void tsfd::dump_goal()
{
    cout << "Goal Conditions:" << endl;
    for(unsigned int i = 0; i < g_goal.size(); i++)
        cout << "  " << g_variable_name[g_goal[i].first] << ": "
            << g_goal[i].second << endl;
}

void tsfd::read_operators(istream &in)
{
    int count;
    in >> count;

    assert(g_initial_state != NULL);

    for(int i = 0; i < count; i++) {
        Operator new_op = Operator(i, in);

        new_op.set_initial_cost(g_initial_state);
        new_op.set_initial_duration(g_initial_state);

        //cout << "Insert operator " << new_op.get_name() << " with acceleration " << new_op.used_acceleration << endl;

        g_operators.push_back(new_op);
    }

    //for(const auto& op : g_operators) {
    //    cout << "Created operator " << op.get_initial_name() << " with acceleration " << op.used_acceleration << " and cost " << op.initial_cost << endl;
    //}

    //cout << "The size of all operators is: " << g_operators.size() << endl;
}

void tsfd::read_logic_axioms(istream &in)
{
    int count;
    in >> count;
    for(int i = 0; i < count; i++) {
        LogicAxiom *ax = new LogicAxiom(in);
        g_axioms.push_back(ax);
    }
}

void tsfd::read_numeric_axioms(istream &in)
{
    int count;
    in >> count;
    for(int i = 0; i < count; i++) {
        NumericAxiom *ax = new NumericAxiom(in);
        g_axioms.push_back(ax);
        // ax->dump();
    }
}

void tsfd::read_contains_universal_conditions(istream &in)
{
    in >> g_contains_universal_conditions;
}

void tsfd::evaluate_axioms_in_init()
{
    g_axiom_evaluator = new AxiomEvaluator;
    g_axiom_evaluator->evaluate(*g_initial_state);
}

void tsfd::read_everything(istream &in)
{
    if(g_parameters.verbose)
        cout << "Read variables" << endl;
    read_variables(in);
    if(g_parameters.verbose)
        cout << "Total: " << g_variable_name.size() << " variables" << endl;

    if(g_parameters.verbose)
        cout << "Read initial state" << endl;
    g_initial_state = new TimeStampedState(in);
    g_initial_state->cost = 0.0;
    if(g_parameters.verbose)
        g_initial_state->dump(true);

    if(g_parameters.verbose)
        cout << "Read goal" << endl;
    read_goal(in);
    if(g_parameters.verbose)
        cout << "Total: " << g_goal.size() << " goal assignments" << endl;

    if(g_parameters.verbose)
        cout << "Read operators" << endl;
    read_operators(in);
    if(g_parameters.verbose)
        cout << "Total: " << g_operators.size() << " operators" << endl;

    if(g_parameters.verbose)
        cout << "Read logic axioms" << endl;
    read_logic_axioms(in);
    int logic = g_axioms.size();
    if(g_parameters.verbose)
        cout << "Total: " << g_axioms.size() << " logic axioms" << endl;

    if(g_parameters.verbose)
        cout << "Read numeric axioms" << endl;
    read_numeric_axioms(in);
    if(g_parameters.verbose)
        cout << "Total: " << g_axioms.size() - logic << " numeric axioms" << endl;

    if(g_parameters.verbose)
        cout << "Evaluate axioms in initial state" << endl;
    evaluate_axioms_in_init();

    if(g_parameters.verbose)
        cout << "Read Successor generator" << endl;
    check_magic(in, "begin_SG");
    g_successor_generator = read_successor_generator(in);
    check_magic(in, "end_SG");

    if(g_parameters.verbose)
        cout << "Read causal graph" << endl;
    g_causal_graph = new CausalGraph(in);

    if(g_parameters.verbose)
        cout << "Read DTG" << endl;
    DomainTransitionGraph::read_all(in);
    read_contains_universal_conditions(in);
}

void tsfd::reset_everything() {
    delete(g_initial_state);
    delete(g_axiom_evaluator);
    delete(g_successor_generator);
    delete(g_causal_graph);
    delete(g_let_time_pass);
    delete(g_wait_operator);

    g_last_arithmetic_axiom_layer = -1;
    g_comparison_axiom_layer = -1;
    g_first_logic_axiom_layer = -1;
    g_last_logic_axiom_layer = -1;
    g_variable_name.clear();
    g_variable_domain.clear();
    g_axiom_layers.clear();
    g_default_axiom_values.clear();
    g_variable_types.clear();
    g_initial_state = NULL;
    g_goal.clear();
    g_operators.clear();
    g_axioms.clear();
    g_axiom_evaluator = NULL;
    g_successor_generator = NULL;
    g_transition_graphs.clear();
    g_causal_graph = NULL;

    g_parameters = PlannerParameters();

    g_let_time_pass = NULL;
    g_wait_operator = NULL;

    g_contains_universal_conditions = false;
}

void tsfd::dump_everything()
{
    cout << "Variables (" << g_variable_name.size() << "):" << endl;
    for(unsigned int i = 0; i < g_variable_name.size(); i++)
        cout << "  " << g_variable_name[i] << " (range "
            << g_variable_domain[i] << ")" << endl;
    cout << "Initial State:" << endl;
    g_initial_state->dump(true);
    dump_goal();
    cout << "Successor Generator:" << endl;
    g_successor_generator->dump();
    for(unsigned int i = 0; i < g_variable_domain.size(); i++)
        g_transition_graphs[i]->dump();
    cout << "Operators:" << endl;
    for(unsigned int i = 0; i < g_operators.size(); ++i)
        g_operators[i].dump();
}

void tsfd::dump_DTGs()
{
    for(unsigned int i = 0; i < g_variable_domain.size(); i++) {
        cout << "DTG of variable " << i;
        g_transition_graphs[i]->dump();
    }
}

int tsfd::g_last_arithmetic_axiom_layer;
int tsfd::g_comparison_axiom_layer;
int tsfd::g_first_logic_axiom_layer;
int tsfd::g_last_logic_axiom_layer;
vector<string> tsfd::g_variable_name;
vector<int> tsfd::g_variable_domain;
vector<int> tsfd::g_axiom_layers;
vector<double> tsfd::g_default_axiom_values;
vector<variable_type> tsfd::g_variable_types;
TimeStampedState *tsfd::g_initial_state;
vector<pair<int, double> > tsfd::g_goal;
vector<Operator> tsfd::g_operators;
vector<Axiom*> tsfd::g_axioms;
AxiomEvaluator *tsfd::g_axiom_evaluator;
SuccessorGenerator *tsfd::g_successor_generator;
vector<DomainTransitionGraph *> tsfd::g_transition_graphs;
CausalGraph *tsfd::g_causal_graph;

PlannerParameters tsfd::g_parameters;

Operator *tsfd::g_let_time_pass;
Operator *tsfd::g_wait_operator;

bool tsfd::g_contains_universal_conditions;

istream& tsfd::operator>>(istream &is, assignment_op &aop)
{
    string strVal;
    is >> strVal;
    if(!strVal.compare("="))
        aop = assign;
    else if(!strVal.compare("+"))
        aop = increase;
    else if(!strVal.compare("-"))
        aop = decrease;
    else if(!strVal.compare("*"))
        aop = scale_up;
    else if(!strVal.compare("/"))
        aop = scale_down;
    else {
        cout << "SEVERE ERROR: expected assignment operator, read in " << strVal << endl;
        assert(false);
    }
    return is;
}

ostream& tsfd::operator<<(ostream &os, const assignment_op &aop)
{
    switch (aop) {
        case assign:
            os << ":=";
            break;
        case scale_up:
            os << "*=";
            break;
        case scale_down:
            os << "/=";
            break;
        case increase:
            os << "+=";
            break;
        case decrease:
            os << "-=";
            break;
        default:
            cout << "Error: aop has value " << (int)aop << endl;

            assert(false);
            break;
    }
    return os;
}

istream& tsfd::operator>>(istream &is, binary_op &bop)
{
    string strVal;
    is >> strVal;
    if(!strVal.compare("+"))
        bop = add;
    else if(!strVal.compare("-"))
        bop = subtract;
    else if(!strVal.compare("*"))
        bop = mult;
    else if(!strVal.compare("/"))
        bop = divis;
    else if(!strVal.compare("<"))
        bop = lt;
    else if(!strVal.compare("<="))
        bop = le;
    else if(!strVal.compare("="))
        bop = eq;
    else if(!strVal.compare(">="))
        bop = ge;
    else if(!strVal.compare(">"))
        bop = gt;
    else if(!strVal.compare("!="))
        bop = ue;
    else {
        cout << strVal << " was read" << endl;
        assert(false);
    }
    return is;
}

ostream& tsfd::operator<<(ostream &os, const binary_op &bop)
{
    switch (bop) {
        case mult:
            os << "*";
            break;
        case divis:
            os << "/";
            break;
        case add:
            os << "+";
            break;
        case subtract:
            os << "-";
            break;
        case lt:
            os << "<";
            break;
        case le:
            os << "<=";
            break;
        case eq:
            os << "=";
            break;
        case ge:
            os << ">=";
            break;
        case gt:
            os << ">";
            break;
        case ue:
            os << "!=";
            break;
        default:
            assert(false);
            break;
    }
    return os;
}

istream& tsfd::operator>>(istream &is, trans_type &tt)
{
    string strVal;
    is >> strVal;
    if(!strVal.compare("s"))
        tt = trans_type::start;
    else if(!strVal.compare("e"))
        tt = trans_type::end;
    else if(!strVal.compare("c"))
        tt = trans_type::compressed;
    else if(!strVal.compare("a"))
        tt = trans_type::ax;
    else {
        cout << strVal << " was read." << endl;
        assert(false);
    }
    return is;
}

ostream& tsfd::operator<<(ostream &os, const trans_type &tt)
{
    switch (tt) {
        case trans_type::start:
            os << "s";
            break;
        case trans_type::end:
            os << "e";
            break;
        case trans_type::compressed:
            os << "c";
            break;
        case trans_type::ax:
            os << "a";
        default:
            // cout << "Error: Encountered binary operator " << bop << "." << endl;
            assert(false);
            break;
    }
    return os;
}

istream& tsfd::operator>>(istream &is, condition_type &ct)
{
    string strVal;
    is >> strVal;
    if(!strVal.compare("s"))
        ct = start_cond;
    else if(!strVal.compare("o"))
        ct = overall_cond;
    else if(!strVal.compare("e"))
        ct = end_cond;
    else if(!strVal.compare("a"))
        ct = ax_cond;
    else
        assert(false);
    return is;
}

ostream& tsfd::operator<<(ostream &os, const condition_type &ct)
{
    switch (ct) {
        case start_cond:
            os << "s";
            break;
        case overall_cond:
            os << "o";
            break;
        case end_cond:
            os << "e";
            break;
        case ax_cond:
            os << "a";
            break;
        default:
            assert(false);
    }
    return os;
}

void tsfd::printSet(const set<int> s)
{
    set<int>::const_iterator it;
    for(it = s.begin(); it != s.end(); ++it)
        cout << *it << ",";
    cout << endl;
}

double tsfd::g_evaluate_expression(const string& expression_string, double a_val)
{
    typedef exprtk::symbol_table<double> symbol_table_t;
    typedef exprtk::expression<double>     expression_t;
    typedef exprtk::parser<double>             parser_t;

    symbol_table_t symbol_table;
    symbol_table.add_variable("a", a_val);
    symbol_table.add_constants();

    expression_t expression;
    expression.register_symbol_table(symbol_table);

    parser_t parser;
    parser.compile(expression_string, expression);

    double out = expression.value();
    return out;
}

double tsfd::g_calculate_cost(const double current_cost, const double action_cost)
{
    double cost = 0.0;
    if(g_parameters.costCalculation == PlannerParameters::ADD) {
        assert(action_cost >= 0);
        cost += current_cost + action_cost;
    } else {
        assert(g_parameters.costCalculation == PlannerParameters::MULTIPLY);
        assert(action_cost >= 0);
        assert(action_cost <= 1.0);

        // We assume error rates here, therefore we calculate the success rate and invert it
        cost = 1.0 - ((1.0 - current_cost) * (1.0 - action_cost));

        assert(cost <= 1.0);
        assert(cost >= 0.0);
        assert(cost >= current_cost);
    }
    //cout << "Increase cost of state with cost " << current_cost << " and " << action_cost << " to " << cost << endl;
    return cost;
}
