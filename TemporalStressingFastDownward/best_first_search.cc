#include "best_first_search.h"

#include "globals.h"
#include "heuristic.h"
#include "successor_generator.h"
#include "planner_parameters.h"
#include <time.h>
#include <iomanip>

#include <cassert>
#include <cmath>

using namespace std;
using namespace tsfd;

OpenListInfo::OpenListInfo(Heuristic *heur, bool only_pref, OpenListMode _mode)
{
    heuristic = heur;
    only_preferred_operators = only_pref;
    mode = _mode;
    priority = 0;
}

void BestFirstSearchEngine::reset() {
    cout << "Resetting the search engine!" << endl;
    closed_list.clear();
    numberOfSearchSteps = 0;
    lastProgressAtExpansionNumber = 0;
    assert(activeQueue < open_lists.size());
    for(unsigned int i = 0; i < open_lists.size(); ++i) {
        open_lists[i].priority = 0;
    }
    current_state = *g_initial_state;
    current_predecessor = NULL;
    current_operator = NULL;
    for(unsigned int i = 0; i < open_lists.size(); ++i) {
        open_lists[i].open = OpenList();
    }
    for(unsigned int i = 0; i < best_heuristic_values_of_queues.size(); ++i) {
        best_heuristic_values_of_queues[i] = -1;
    }
    queueStartedLastWith = (queueStartedLastWith+1) % open_lists.size();
    assert(queueStartedLastWith < open_lists.size());
    cout << "Giving prior boost to open list " << queueStartedLastWith << endl;
    open_lists[queueStartedLastWith].priority -= g_parameters.open_list_initial_boost;

    cout << "Open list sizes (priorities):";
    for(unsigned int i = 0; i < open_lists.size(); ++i) {
        cout << " " << open_lists[i].open.size() << " (" << open_lists[i].priority << "), ";
    }
    cout << endl;
}

BestFirstSearchEngine::BestFirstSearchEngine() :
        number_of_expanded_nodes(0), current_state(*g_initial_state)
{
    current_predecessor = NULL;
    current_operator = NULL;
    start_time = time(NULL);
    bestMakespan = HUGE_VAL;
    maximumMakespan = HUGE_VAL;
    maximumCost = HUGE_VAL;
    bestCost = HUGE_VAL;
    bestSumOfGoals = HUGE_VAL;
    queueStartedLastWith = 0;
}

BestFirstSearchEngine::~BestFirstSearchEngine()
{
}

void BestFirstSearchEngine::add_heuristic(Heuristic *heuristic, bool use_estimates, bool only_use_preferred_operators)
{
    assert(use_estimates || only_use_preferred_operators);
    if(only_use_preferred_operators) {
        //best_heuristic_values_of_queues.push_back(-1);
        preferred_operator_heuristics_reg.push_back(heuristic);
        open_lists.push_back(OpenListInfo(heuristic, true, REGULAR));
    }
    heuristics.push_back(heuristic);
    best_heuristic_values_of_queues.push_back(-1);
    if(use_estimates) {
        open_lists.push_back(OpenListInfo(heuristic, false, ALL));
    }
}

void BestFirstSearchEngine::initialize()
{
    assert(!open_lists.empty());
    activeQueue = 0;
    lastProgressAtExpansionNumber = 0;
    numberOfSearchSteps = 0;
    cout << "Open list sizes (priorities):";
    for(unsigned int i = 0; i < open_lists.size(); ++i) {
        cout << " " << open_lists[i].open.size() << " (" << open_lists[i].priority << "), ";
    }
    cout << endl;
}

void BestFirstSearchEngine::statistics(time_t & current_time)
{
    cout << endl;
    cout << "Search Time: " << (current_time - start_time) << " sec." << endl;
    search_statistics.dump(number_of_expanded_nodes, current_time);
    cout << "OpenList sizes:";
    for(unsigned int i = 0; i < open_lists.size(); ++i) {
        cout << " " << open_lists[i].open.size();
    }
    cout << endl;
    cout << "Heuristic Computations (per heuristic):";
    unsigned long totalHeuristicComputations = 0;
    for(unsigned int i = 0; i < heuristics.size(); ++i) {
        Heuristic *heur = heuristics[i];
        totalHeuristicComputations += heur->get_num_computations();
        cout << " " << heur->get_num_computations();
    }
    cout << " Total: " << totalHeuristicComputations << endl;
    cout << "Number of cache hits in heuristic (per heuristic):";
    unsigned long totalHeuristicCacheHits = 0;
    for(unsigned int i = 0; i < heuristics.size(); ++i) {
        Heuristic *heur = heuristics[i];
        totalHeuristicCacheHits += heur->get_num_cache_hits();
        cout << " " << heur->get_num_cache_hits();
    }
    cout << " Total: " << totalHeuristicCacheHits << endl;

    cout << "Best heuristic values of queues:";
    for(unsigned int i = 0; i < best_heuristic_values_of_queues.size(); ++i) {
        cout << " [" << i << ": " << best_heuristic_values_of_queues[i] << "]";
    }
    cout << endl;
    cout << "Best heuristic priorities of queues:";
    for(unsigned int i = 0; i < open_lists.size(); ++i) {
        cout << " [" << i << ": " << open_lists[i].priority << "]";
    }
    cout << endl << endl;

}

void BestFirstSearchEngine::dump_transition() const
{
    cout << endl;
    if(current_predecessor != 0) {
        cout << "DEBUG: In step(), current predecessor is: " << endl;
        current_predecessor->dump(g_parameters.verbose);
    }
    cout << "DEBUG: In step(), current operators are: ";
    cout << "  " << current_operator->get_name();
    cout << endl;
    cout << "DEBUG: In step(), current state is: " << endl;
    current_state.dump(g_parameters.verbose);
    cout << endl;
}

void BestFirstSearchEngine::dump_everything() const
{
    dump_transition();
    cout << endl << endl;
    for (std::vector<OpenListInfo>::const_iterator it = open_lists.begin(); it
            != open_lists.end(); it++) {
        cout << "DEBUG: an open list is:" << endl;
        // huge dangerous hack...
        OpenListEntry const* begin = &(it->open.top());
        OpenListEntry const* end = &(it->open.top()) + it->open.size();
        for (const OpenListEntry* it2 = begin; it2 != end; it2++) {
            cout << "OpenListEntry" << endl;
            cout << "state" << endl;
            std::tr1::get<0>(*it2)->dump(true);
            cout << "ops: ";
            const Operator* op = std::tr1::get<1>(*it2);
            cout << "  " << op->get_name();
            cout << endl;
            double h = std::tr1::get<2>(*it2);
            cout << "Value: " << h << endl;
            cout << "end OpenListEntry" << endl;
        }
    }
}

SearchEngine::status BestFirstSearchEngine::step()
{
    // Invariants:
    // - current_state is the next state for which we want to compute the heuristic.
    // - current_predecessor is a permanent pointer to the predecessor of that state.
    // - current_operator is the operator which leads to current_state from predecessor.


    //cout << "SKip all states with ts > "  << this->maximumMakespan << " and cost > " << this->maximumCost << endl;

    /*cout << "STEP" << endl;
    cout << "Check current state" << endl;
    current_state.dump(true);
    cout << "Open list size: " << this->open_lists[0].open.size() << endl;*/


    numberOfSearchSteps++;

    bool discard = true;

    double maxTimeIncrement = 0.0;
    for(unsigned int k = 0; k < current_state.operators.size(); ++k) {
        maxTimeIncrement = max(maxTimeIncrement, current_state.operators[k].time_increment);
    }
    double makeSpan = maxTimeIncrement + current_state.timestamp;
    double cost = current_state.cost;

    //cout << "Current state has cost " << cost << endl;

    if(g_parameters.g_values == PlannerParameters::GCost) {
        if(cost < bestCost) {
            discard = false;
        }
    } else {
        assert(g_parameters.g_values == PlannerParameters::GMakespan);
        if(makeSpan < bestMakespan) {
            discard = false;
        }
    }

    if(!discard && cost > maximumCost) {
        discard = true;
    }

    if(!discard && (makeSpan > maximumMakespan || closed_list.contains(current_state))) {
        //cout << "Discard because state is closed" << endl;
        discard = true;
    }

    // throw away any states resulting from zero cost actions (can't handle)
    if(!discard) {
        if(current_predecessor && current_operator && current_operator != g_let_time_pass && current_operator->get_duration(current_predecessor) <= 0.0) {
            discard = true;
        }
    }

    if(!discard) {
        const TimeStampedState* parent_ptr = NULL;
        if(current_operator == NULL) { // Initial state
            assert(current_predecessor != & current_state);
            number_of_expanded_nodes++;
            parent_ptr = closed_list.insert(current_state, current_predecessor, NULL);
        } else {
            assert(current_state.is_consistent_when_progressed());
            assert(current_operator->get_name().compare("wait") != 0);
            assert(current_predecessor != &current_state);
            number_of_expanded_nodes++;
            parent_ptr = closed_list.insert(current_state, current_predecessor, current_operator);
        }


        //cout << "Checking state" << endl;
        //current_state.dump(true);

        // evaluate the current/parent state
        // also do this for non/lazy-evaluation as the heuristics
        // provide preferred operators!
        for(unsigned int i = 0; i < heuristics.size(); i++) {
            heuristics[i]->evaluate(current_state);
        }
        if(!is_dead_end()) {
            if(check_progress()) {
                // current_state.dump();
                report_progress();
                reward_progress();
            }
            if(check_goal()) {
                return SOLVED;
            }
            generate_successors(parent_ptr);
        } else {
            //cout << "It's a dead end" << endl;
        }
    } else {
        //cout << "Discard that state" << endl;
        if ((current_operator == g_let_time_pass) && current_state.operators.empty() && makeSpan < bestMakespan) {
            // arrived at same state by letting time pass
            // e.g. when having an action with duration and only start effect
            // the result would be discarded, check if we are at goal
            for(unsigned int i = 0; i < heuristics.size(); i++) {
                heuristics[i]->evaluate(current_state);
            }
            if(!is_dead_end()) {
                if(check_goal())
                    return SOLVED;
            }
        }
    }

    time_t current_time = time(NULL);
    static time_t last_stat_time = current_time;
    if(g_parameters.verbose && current_time - last_stat_time >= 10) {
        statistics(current_time);
        last_stat_time = current_time;
    }

    // use different timeouts depending if we found a plan or not.
    if(found_solution()) {
        if (g_parameters.timeout_if_plan_found > 0 
                && current_time - start_time > g_parameters.timeout_if_plan_found) {
            if(g_parameters.verbose)
                statistics(current_time);
            return SOLVED_TIMEOUT;
        }
    } else {
        if (g_parameters.timeout_while_no_plan_found > 0 
                && current_time - start_time > g_parameters.timeout_while_no_plan_found) {
            if(g_parameters.verbose)
                statistics(current_time);
            return FAILED_TIMEOUT;
        }
    }

    return fetch_next_state();
}

bool BestFirstSearchEngine::is_dead_end()
{
    // If a reliable heuristic reports a dead end, we trust it.
    // Otherwise, all heuristics must agree on dead-end-ness.
    unsigned int dead_end_counter = 0;
    for(unsigned int i = 0; i < heuristics.size(); i++) {
        if(heuristics[i]->is_dead_end()) {
            if(heuristics[i]->dead_ends_are_reliable())
                return true;
            else
                dead_end_counter++;
        }
    }
    return dead_end_counter == heuristics.size();
}

bool BestFirstSearchEngine::check_goal()
{
    // Any heuristic reports 0 iff this is a goal state, so we can
    // pick an arbitrary one. Heuristics are assumed to not miss a goal
    // state and especially not to report a goal as dead end.
    Heuristic *heur = open_lists[0].heuristic;
 
    // We actually need this silly !heur->is_dead_end() check because
    // this state *might* be considered a non-dead end by the
    // overall search even though heur considers it a dead end
    // (e.g. if heur is the CG heuristic, but the FF heuristic is
    // also computed and doesn't consider this state a dead end.
    // If heur considers the state a dead end, it cannot be a goal
    // state (heur will not be *that* stupid). We may not call
    // get_heuristic() in such cases because it will barf.
    if(!heur->is_dead_end() && heur->get_heuristic() == 0) {
        if(current_state.operators.size() > 0) {
            return false;
        }
        // found goal

        if(!current_state.satisfies(g_goal)) {  // will assert...
            dump_everything();
        }
        assert(current_state.operators.empty() && current_state.satisfies(g_goal));

        Plan plan;
        PlanTrace path;
        closed_list.trace_path(current_state, plan, path);
        set_plan(plan);
        set_path(path);
        return true;
    } else {
        return false;
    }
}

void BestFirstSearchEngine::dump_plan_prefix_for_current_state() const
{
    dump_plan_prefix_for__state(current_state);
}

void BestFirstSearchEngine::dump_plan_prefix_for__state(const TimeStampedState &state) const
{
    Plan plan;
    PlanTrace path;
    closed_list.trace_path(state, plan, path);
    for(unsigned int i = 0; i < plan.size(); i++) {
        const PlanStep& step = plan[i];
        cout << step.start_time << ": " << "(" << step.op->get_name() << ")"
            << " [" << step.duration << "]" << endl;
    }
}

bool BestFirstSearchEngine::check_progress()
{
    assert(activeQueue < best_heuristic_values_of_queues.size());
    assert(activeQueue < open_lists.size());
    Heuristic* heuristic = open_lists[activeQueue].heuristic;
    if(heuristic->is_dead_end())
        return false;
    double h = heuristic->get_heuristic();

    //cout << "Current state has estimated distance : " << h << endl;

    double &best_h = best_heuristic_values_of_queues[activeQueue];
    if(best_h == -1 || h < best_h) {
        best_h = h;
        return true;
    }
    return false;
}

void BestFirstSearchEngine::report_progress()
{
    cout << "Best heuristic values of queues: ";
    for(unsigned int i = 0; i < best_heuristic_values_of_queues.size(); i++) {
        cout << best_heuristic_values_of_queues[i];
        if(i != best_heuristic_values_of_queues.size() - 1)
            cout << "/";
    }
    cout << " [expanded " << closed_list.size() << " state(s)]" << endl;
}

void BestFirstSearchEngine::reward_progress()
{
    // Boost the "preferred operator" open lists somewhat whenever
    // progress is made. This used to be used in multi-heuristic mode
    // only, but it is also useful in single-heuristic mode, at least
    // in Schedule.
    //
    // Future Work: Test the impact of this, and find a better way of rewarding
    // successful exploration. For example, reward only the open queue
    // from which the good state was extracted and/or the open queues
    // for the heuristic for which a new best value was found.

    assert(activeQueue < open_lists.size());
    open_lists[activeQueue].priority -= g_parameters.open_list_boost;
    lastProgressAtExpansionNumber = numberOfSearchSteps;
}

const Operator* BestFirstSearchEngine::get_fastest_accelerated_op(const Operator* op) const
{
    int index = op->get_index();

    const Operator* best_accelerated = op;
    for(unsigned int i = index+1; i < g_operators.size(); ++i) {
        if(g_operators[i].get_initial_name() != op->get_initial_name()) {
            break;
        }

        if(g_operators[i].used_acceleration > best_accelerated->used_acceleration) {
            best_accelerated = &g_operators[i];
        }
    }

    return best_accelerated;
}

const Operator* BestFirstSearchEngine::get_cheapest_accelerated_op(const Operator* op) const
{
    int index = op->get_index();

    const Operator* best_accelerated = op;
    for(unsigned int i = index+1; i < g_operators.size(); ++i) {
        if(g_operators[i].get_initial_name() != op->get_initial_name()) {
            break;
        }

        if(g_operators[i].initial_cost < best_accelerated->initial_cost) {
            best_accelerated = &g_operators[i];
        }
    }

    return best_accelerated;
}

std::vector<const Operator *> BestFirstSearchEngine::extend_to_accelerated_ops(const std::vector<const Operator *>& ops) const
{
    std::vector<const Operator*> extended_set;
    for (const auto& op : ops) {
        int start_index = op->get_index();
        assert(start_index >= 0);

        if(g_parameters.g_values == PlannerParameters::GMakespan && this->maximumMakespan == HUGE_VAL && this->maximumCost == HUGE_VAL) {
            const Operator* best_accelerated = this->get_fastest_accelerated_op(op);
            extended_set.push_back(best_accelerated);
            continue;
        }
        if(g_parameters.g_values == PlannerParameters::GCost && this->maximumCost == HUGE_VAL && this->maximumMakespan == HUGE_VAL) {
            const Operator* best_accelerated = this->get_cheapest_accelerated_op(op);
            extended_set.push_back(best_accelerated);
            continue;
        }

        extended_set.push_back(op);
        for(unsigned int i = start_index+1; i < g_operators.size(); ++i) {
            if(g_operators[i].get_initial_name() != op->get_initial_name()) {
                break;
            }

            extended_set.push_back(&g_operators[i]);
        }
    }

    return extended_set;
}

void BestFirstSearchEngine::generate_successors(const TimeStampedState *parent_ptr)
{
    vector<const Operator *> all_operators;
    g_successor_generator->generate_applicable_ops(*parent_ptr, all_operators);
    // Filter ops that cannot be applicable just from the preprocess data (doesn't guarantee full applicability)

    //for(const auto& op : all_operators) {
    //    cout << "Generate successor with op: " << op->get_name() << endl;
    //}

    vector<const Operator *> preferred_operators_reg;
    for(unsigned int i = 0; i < preferred_operator_heuristics_reg.size(); i++) {
        Heuristic *heur = preferred_operator_heuristics_reg[i];
        if(!heur->is_dead_end()) {
            heur->get_preferred_operators(preferred_operators_reg,REGULAR);
        }
    }
    //cout << "Prefered op reg size is " << preferred_operators_reg .size() << endl;

    vector<const Operator *> preferred_operators_ordered;
    for(unsigned int i = 0; i < preferred_operator_heuristics_ordered.size(); i++) {
        Heuristic *heur = preferred_operator_heuristics_ordered[i];
        if(!heur->is_dead_end()) {
            heur->get_preferred_operators(preferred_operators_ordered,ORDERED);
        }
    }
    //cout << "Prefered op ord size is " << preferred_operators_ordered .size() << endl;

    vector<const Operator *> preferred_operators_cheapest;
    for(unsigned int i = 0; i < preferred_operator_heuristics_cheapest.size(); i++) {
        Heuristic *heur = preferred_operator_heuristics_cheapest[i];
        if(!heur->is_dead_end()) {
            heur->get_preferred_operators(preferred_operators_cheapest,CHEAPEST);
        }
    }
    //cout << "Prefered op che size is " << preferred_operators_cheapest .size() << endl;

    vector<const Operator *> preferred_operators_most_expensive;
    for(unsigned int i = 0; i < preferred_operator_heuristics_most_expensive.size(); i++) {
        Heuristic *heur = preferred_operator_heuristics_most_expensive[i];
        if(!heur->is_dead_end()) {
            heur->get_preferred_operators(preferred_operators_most_expensive,MOSTEXPENSIVE);
        }
    }
    //cout << "Prefered op mostexp size is " << preferred_operators_most_expensive .size() << endl;

    vector<const Operator *> preferred_operators_rand;
    for(unsigned int i = 0; i < preferred_operator_heuristics_rand.size(); i++) {
        Heuristic *heur = preferred_operator_heuristics_rand[i];
        if(!heur->is_dead_end()) {
            heur->get_preferred_operators(preferred_operators_rand,RAND);
        }
    }
    //cout << "Prefered op rand size is " << preferred_operators_rand .size() << endl;

    vector<const Operator *> preferred_operators_concurrent;
    //    cout << "preferred_operator_heuristics_concurrent.size(): " << preferred_operator_heuristics_concurrent.size() << endl;
    for(unsigned int i = 0; i < preferred_operator_heuristics_concurrent.size(); i++) {
        Heuristic *heur = preferred_operator_heuristics_concurrent[i];
        if(!heur->is_dead_end()) {
            heur->get_preferred_operators(preferred_operators_concurrent,CONCURRENT);
        }
    }
    //cout << "Prefered op conc size is " << preferred_operators_concurrent .size() << endl;

    vector<const Operator *> preferred_applicable_operators_reg;
    vector<const Operator *> preferred_applicable_operators_ordererd;
    vector<const Operator *> preferred_applicable_operators_cheapest;
    vector<const Operator *> preferred_applicable_operators_most_expensive;
    vector<const Operator *> preferred_applicable_operators_rand;
    vector<const Operator *> preferred_applicable_operators_concurrent;
    // all_operators contains a superset of applicable operators based on preprocess data
    // this in not necessarily all operators in the task, but also doesn't guarantee applicablity, yet.
    // Here we split this superset of operators in two:
    // - preferred_applicable_operators will be those operators from all_operators that are preferred_operators
    // - all_operators will be the rest (i.e. the non-preferred operators)
    // preferred_applicable_operators thus contains all operators that are preferred and might be applicable,
    // i.e. a subset of preferred_operators
    // The preferred_operators to be used will thus be only the preferred_applicable_operators
    for(unsigned int k = 0; k < preferred_operators_reg.size(); ++k) {
        for(unsigned int l = 0; l < all_operators.size(); ++l) {
            if(all_operators[l] == preferred_operators_reg[k]) {
                preferred_applicable_operators_reg.push_back(preferred_operators_reg[k]);
                break;
            }
        }
    }
    preferred_operators_reg = this->extend_to_accelerated_ops(preferred_applicable_operators_reg);
    for(unsigned int k = 0; k < preferred_operators_ordered.size(); ++k) {
        for(unsigned int l = 0; l < all_operators.size(); ++l) {
            if(all_operators[l] == preferred_operators_ordered[k]) {
                preferred_applicable_operators_ordererd.push_back(preferred_operators_ordered[k]);
                break;
            }
        }
    }
    preferred_operators_ordered = this->extend_to_accelerated_ops(preferred_applicable_operators_ordererd);

    for(unsigned int k = 0; k < preferred_operators_cheapest.size(); ++k) {
        for(unsigned int l = 0; l < all_operators.size(); ++l) {
            if(all_operators[l] == preferred_operators_cheapest[k]) {
                preferred_applicable_operators_cheapest.push_back(preferred_operators_cheapest[k]);
                break;
            }
        }
    }
    preferred_operators_cheapest = this->extend_to_accelerated_ops(preferred_applicable_operators_cheapest);

    for(unsigned int k = 0; k < preferred_operators_most_expensive.size(); ++k) {
        for(unsigned int l = 0; l < all_operators.size(); ++l) {
            if(all_operators[l] == preferred_operators_most_expensive[k]) {
                preferred_applicable_operators_most_expensive.push_back(preferred_operators_most_expensive[k]);
                break;
            }
        }
    }
    preferred_operators_most_expensive = this->extend_to_accelerated_ops(preferred_applicable_operators_most_expensive);

    for(unsigned int k = 0; k < preferred_operators_rand.size(); ++k) {
        for(unsigned int l = 0; l < all_operators.size(); ++l) {
            if(all_operators[l] == preferred_operators_rand[k]) {
                preferred_applicable_operators_rand.push_back(preferred_operators_rand[k]);
                break;
            }
        }
    }
    preferred_operators_rand = this->extend_to_accelerated_ops(preferred_applicable_operators_rand);

    for(unsigned int k = 0; k < preferred_operators_concurrent.size(); ++k) {
        for(unsigned int l = 0; l < all_operators.size(); ++l) {
            if(all_operators[l] == preferred_operators_concurrent[k]) {
                preferred_applicable_operators_concurrent.push_back(preferred_operators_concurrent[k]);
                break;
            }
        }
    }
    preferred_operators_concurrent = preferred_applicable_operators_concurrent;

    for(unsigned int l = 0; l < all_operators.size(); ++l) {
    	bool deleted = false;
        for(unsigned int k = 0; k < preferred_operators_ordered.size(); ++k) {
    		if(all_operators[l] == preferred_operators_ordered[k]) {
    			all_operators[l] = all_operators[all_operators.size() - 1];
    			all_operators.pop_back();
    			deleted = true;
    			break;
    		}
        }
    	if(deleted) {
    		continue;
    	}
        for(unsigned int k = 0; k < preferred_operators_reg.size(); ++k) {
    		if(all_operators[l] == preferred_operators_reg[k]) {
    			all_operators[l] = all_operators[all_operators.size() - 1];
    			all_operators.pop_back();
    			deleted = true;
    			break;
    		}
        }
    	if(deleted) {
    		continue;
    	}
        for(unsigned int k = 0; k < preferred_operators_concurrent.size(); ++k) {
    		if(all_operators[l] == preferred_operators_concurrent[k]) {
    			all_operators[l] = all_operators[all_operators.size() - 1];
    			all_operators.pop_back();
    			deleted = true;
    			break;
    		}
    	}
        if(deleted) {
            continue;
        }
        for(unsigned int k = 0; k < preferred_operators_cheapest.size(); ++k) {
            if(all_operators[l] == preferred_operators_cheapest[k]) {
                all_operators[l] = all_operators[all_operators.size() - 1];
                all_operators.pop_back();
                break;
            }
        }
        if(deleted) {
            continue;
        }
        for(unsigned int k = 0; k < preferred_operators_most_expensive.size(); ++k) {
            if(all_operators[l] == preferred_operators_most_expensive[k]) {
                all_operators[l] = all_operators[all_operators.size() - 1];
                all_operators.pop_back();
                break;
            }
        }
        if(deleted) {
            continue;
        }
        for(unsigned int k = 0; k < preferred_operators_rand.size(); ++k) {
            if(all_operators[l] == preferred_operators_rand[k]) {
                all_operators[l] = all_operators[all_operators.size() - 1];
                all_operators.pop_back();
                break;
            }
        }
    }

    for(unsigned int i = 0; i < open_lists.size(); i++) {
        //cout << "Expanding for open list " << i << endl;

        Heuristic *heur = open_lists[i].heuristic;

        double priority = -1;   // invalid
        // lazy eval = compute priority by parent
        if(g_parameters.lazy_evaluation) {
            double parentG = getG(parent_ptr, parent_ptr, NULL);
            double parentH = heur->get_heuristic();
            assert(!heur->is_dead_end());
            double parentF = parentG + parentH;
            if(g_parameters.greedy)
                priority = parentH;
            else
                priority = parentF;
        }

        OpenList &open = open_lists[i].open;
        vector<const Operator *>* ops = NULL;
        OpenListMode& mode = open_lists[i].mode;
        switch (mode) {
        case ALL:
            assert(!(open_lists[i].only_preferred_operators));
            ops = &all_operators;
            break;
        case REGULAR:
            assert(open_lists[i].only_preferred_operators);
            ops = &preferred_operators_reg;
            break;
        case CHEAPEST:
            assert(open_lists[i].only_preferred_operators);
            ops = &preferred_operators_cheapest;
            break;
        case MOSTEXPENSIVE:
            assert(open_lists[i].only_preferred_operators);
            ops = &preferred_operators_most_expensive;
            break;
        case RAND:
            assert(open_lists[i].only_preferred_operators);
            ops = &preferred_operators_rand;
            break;
        case ORDERED:
            assert(open_lists[i].only_preferred_operators);
            ops = &preferred_operators_ordered;
            break;
        case CONCURRENT:
            ops = &preferred_operators_concurrent;
            break;
        default:
            assert(false);
            break;
        }

        assert(ops);

        //cout << "We have " << ops->size() << " ops to schedule" << endl;

        // push successors from applicable ops
        //cout << "Expanding state" << endl;
        //parent_ptr->dump(true);

        for(unsigned int j = 0; j < ops->size(); j++) {
            assert((*ops)[j]->get_name().compare("wait") != 0);

            //cout << "Try to append op " << (*ops)[j]->get_name() << endl;

            // compute expected min makespan of this op
            double maxTimeIncrement = 0.0;
            for(unsigned int k = 0; k < parent_ptr->operators.size(); ++k) {
                maxTimeIncrement = max(maxTimeIncrement, parent_ptr->operators[k].time_increment);
            }
            double duration = (*ops)[j]->get_duration(parent_ptr);
            double cost = (*ops)[j]->get_cost(parent_ptr);
            maxTimeIncrement = max(maxTimeIncrement, duration);
            double makespan = maxTimeIncrement + parent_ptr->timestamp;
            double statecost = parent_ptr->cost + cost;
            bool betterMakespan = makespan < bestMakespan;
            bool betterCost = statecost < bestCost;

            // Generate a child/Use an operator if
            // - it is applicable
            // - its minimum makespan is better than the best we had so far
            // - if knownByLogicalStateOnly hasn't closed this state (when feature enabled)

            if(makespan < maximumMakespan && cost < maximumCost && ((g_parameters.g_values == PlannerParameters::GMakespan && betterMakespan) || (g_parameters.g_values == PlannerParameters::GCost && betterCost)) && (*ops)[j]->is_applicable(*parent_ptr)) {
                // non lazy eval = compute priority by child
                if(!g_parameters.lazy_evaluation) {
                    // need to compute the child to evaluate it
                    TimeStampedState tss = TimeStampedState(*parent_ptr, *(*ops)[j]);
                    double childG = getG(&tss, parent_ptr, (*ops)[j]);
                    double childH = heur->evaluate(tss);
                    if(heur->is_dead_end())
                        continue;
                    double childF = childG + childH;
                    if(g_parameters.greedy)
                        priority = childH;
                    else
                        priority = childF;
                }

                //cout << "Schedule action " << (*ops)[j]->get_name() << "with acc = " << (*ops)[j]->used_acceleration << " to start now" << endl;
                //cout << "The new state has heuristic f=" << priority << endl;

                open.push(std::tr1::make_tuple(parent_ptr, (*ops)[j], priority, parent_ptr->timestamp));
                search_statistics.countChild(i);

                //cout << "Inserted new state with operator " << (*ops)[j]->get_name() << " and f = " << priority << endl;

                // also add a new operator to end simultanously to next scheduled operator
                if(g_parameters.future_scheduling && parent_ptr->scheduled_effects.size() > 0) {
                    double nh = parent_ptr->next_happening();

                    double action_starts_at = nh - duration;

                    if(action_starts_at == parent_ptr->timestamp) {
                        continue; // should be scheduled already
                    }

                    if(action_starts_at > parent_ptr->timestamp) {
                        //cout << ">>>> Schedule action " << (*ops)[j]->get_name() << " to start in the future: " << action_starts_at << endl;

                        open.push(std::tr1::make_tuple(parent_ptr, (*ops)[j], priority, action_starts_at - EPSILON)); // schedule to end shortly before nh
                        search_statistics.countChild(i);

                        open.push(std::tr1::make_tuple(parent_ptr, (*ops)[j], priority, action_starts_at + EPSILON)); // schedule to end shortly after nh
                        search_statistics.countChild(i);
                    }
                }
            }

            // schedule an action to the past
            if(g_parameters.past_scheduling && parent_ptr->scheduled_effects.size() > 0) {
                double nh = parent_ptr->next_happening();

                double action_starts_at = nh - duration;

                if(action_starts_at < parent_ptr->timestamp) {
                    if(action_starts_at < 0.0) { // Do not schedule earlier than 0.0
                        //cout << ">>>> cant past schedule action " << (*ops)[j]->get_name() << endl;
                        continue;
                    }

                    // schedule to the past by first getting past node
                    //cout << ">>>> Schedule action " << (*ops)[j]->get_name() << " to start in the past: " << action_starts_at << endl;
                    const TimeStampedState* past_parent = parent_ptr;
                    while(past_parent->timestamp > action_starts_at) {
                        const auto *pre_info = this->closed_list.get_predecessor(*past_parent);
                        assert(pre_info != NULL);

                        past_parent = pre_info->predecessor;
                    }

                    if(past_parent->timestamp == action_starts_at) {
                        continue; // should already be scheduled
                    }

                    //cout << "Found a parent state with ts < " << action_starts_at << endl;
                    //cout << "Namely " << endl;
                    //past_parent->dump(true);
                    //cout << "EOS" << endl;

                    assert(past_parent->next_happening() > action_starts_at);

                    // compute expected min makespan of this op
                    double maxTimeIncrement = 0.0;
                    for(unsigned int k = 0; k < past_parent->operators.size(); ++k) {
                        maxTimeIncrement = max(maxTimeIncrement, past_parent->operators[k].time_increment);
                    }
                    maxTimeIncrement = max(maxTimeIncrement, duration);
                    double makespan = maxTimeIncrement + past_parent->timestamp;
                    double statecost = past_parent->cost;
                    bool betterMakespan = makespan < bestMakespan;
                    bool betterCost = statecost <  bestCost;

                    // only compute tss if needed
                    if(makespan < maximumMakespan && cost < maximumCost && ((g_parameters.g_values == PlannerParameters::GMakespan && betterMakespan) || (g_parameters.g_values == PlannerParameters::GCost && betterCost)) &&
                            (*ops)[j]->is_applicable(*past_parent)) {

                        // non lazy eval = compute priority by child
                        if(!g_parameters.lazy_evaluation) {
                            // need to compute the child to evaluate it
                            TimeStampedState tss = TimeStampedState(*past_parent, *(*ops)[j]);
                            double childG = getG(&tss, past_parent, (*ops)[j]);
                            double childH = heur->evaluate(tss);
                            if(heur->is_dead_end())
                                continue;
                            double childF = childG + childH;
                            if(g_parameters.greedy)
                                priority = childH;
                            else
                                priority = childF;
                        }

                        open.push(std::tr1::make_tuple(past_parent, (*ops)[j], priority, action_starts_at));
                        search_statistics.countChild(i);
                    }
                }
            }
        }

        // Inserted all children, now insert one more child by letting time pass
        // only allow let_time_pass if there are running operators (i.e. there is time to pass)
        if(!parent_ptr->operators.empty()) {
            // non lazy eval = compute priority by child
            if(!g_parameters.lazy_evaluation) {
                // compute child
                TimeStampedState tss = parent_ptr->let_time_pass(HUGE_VAL,true);
                double childG = getG(&tss, parent_ptr, NULL);
                double childH = heur->evaluate(tss);
                if(heur->is_dead_end()) {
                    continue;
                }

                double childF = childH + childG;
                if(g_parameters.greedy)
                    priority = childH;
                else
                    priority = childF;
            }
            open.push(std::tr1::make_tuple(parent_ptr, g_let_time_pass, priority, parent_ptr->timestamp));

            //cout << "Inserted new state with operator let_time_pass and f = " << priority << endl;
            //cout << "The new state has heuristic f=" << priority << endl;

            search_statistics.countChild(i);
        }
    }

    search_statistics.finishExpansion();
}

enum SearchEngine::status BestFirstSearchEngine::fetch_next_state()
{
    OpenListInfo *open_info = select_open_queue();
    if(!open_info) {
        if(found_at_least_one_solution()) {
            cout << "Completely explored state space -- best plan found!" << endl;
            return SOLVED_COMPLETE;
        }
        
        if(g_parameters.verbose) {
            time_t current_time = time(NULL);
            statistics(current_time);
        }
        cout << "Completely explored state space -- no solution!" << endl;
        return FAILED;
    }

    std::tr1::tuple<const TimeStampedState *, const Operator*, double, double> next = open_info->open.top();
    open_info->open.pop();
    open_info->priority++;

    current_predecessor = std::tr1::get<0>(next);
    current_operator = std::tr1::get<1>(next);
    double start_at = std::tr1::get<3>(next);
    double start_in = start_at - current_predecessor->timestamp;

    //cout << "Fetching next state by using operator ";
    //cout << current_operator->get_name();
    //cout << " to start in " << start_in << endl;

    assert(start_in >= 0.0);

    if(current_operator == g_let_time_pass) {
        assert(start_in == 0.0);
        double nh = current_predecessor->next_happening();
        double nh_starts_in = nh - current_predecessor->timestamp;
        assert(nh_starts_in >= 0);
        current_state = current_predecessor->let_time_pass(nh_starts_in, true);
    } else {
        assert(current_operator != g_let_time_pass);

        //Apply the operator.
        assert(current_operator->get_name().compare("wait") != 0);
        assert(current_operator->is_applicable(*current_predecessor));

        if(start_at > 0.0) {
            double nh = current_predecessor->next_happening();
            double nh_starts_in = nh - current_predecessor->timestamp;
            assert(start_in <= nh_starts_in);
            const TimeStampedState new_predecessor = current_predecessor->let_time_pass(start_in, true);
            current_state = TimeStampedState(new_predecessor, *current_operator);
        } else {
            current_state = TimeStampedState(*current_predecessor, *current_operator);
        }
    }

    assert(&current_state != current_predecessor);
    return IN_PROGRESS;
}

OpenListInfo *BestFirstSearchEngine::select_open_queue()
{
    OpenListInfo *best = 0;

    for(unsigned int i = 0; i < open_lists.size(); i++) {
        if(!open_lists[i].open.empty() && (best == 0 || open_lists[i].priority < best->priority)) {
            best = &open_lists[i];
            activeQueue = i;
        }
    }

    return best;
}

double BestFirstSearchEngine::getGc(const TimeStampedState *state) const
{
    return state->cost;
}

double BestFirstSearchEngine::getGc(const TimeStampedState *state, const Operator *op) const
{
    double opCost = 0.0;
    if (op && op != g_let_time_pass) {
        opCost += op->get_cost(state);
    }
    return getGc(state) + opCost;
}

double BestFirstSearchEngine::getGm(const TimeStampedState *state) const
{
    double longestActionDuration = 0.0;
    for (unsigned int i = 0; i < state->operators.size(); ++i) {
        const ScheduledOperator* op = &state->operators[i];
        double duration = 0.0;
        if (op && op != g_let_time_pass) {
            duration = op->get_duration(state);
        }
        if (duration > longestActionDuration) {
            longestActionDuration = duration;
        }
    }
    return state->timestamp + longestActionDuration;
}

/**
 * If mode is cost or weighted a parent_ptr and op have to be given.
 *
 * \param [in] state_ptr the state to compute G for
 * \param [in] closed_ptr should be a closed node that op could be applied to, if state is not closed (i.e. a child)
 */
double BestFirstSearchEngine::getG(const TimeStampedState* state_ptr, 
        const TimeStampedState* closed_ptr, const Operator* op) const
{
    double g = HUGE_VAL;
    switch(g_parameters.g_values) {
        case PlannerParameters::GCost:
            if(op == NULL)
                g = getGc(closed_ptr);
            else
                g = getGc(closed_ptr, op);
            break;
        case PlannerParameters::GMakespan:
            g = getGm(state_ptr);
            break;
        default:
            assert(false);
    }
    return g;
}

