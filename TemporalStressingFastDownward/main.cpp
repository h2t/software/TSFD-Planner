#include "temporal_stressing_planner_controller.h"

#include <iostream>
#include <string>
#include <fstream>

using namespace std;
using namespace tsfd;

// THIS MAIN FILE IS ONLY FOR TESTING PURPOSE
int main(int argc, char* argv[])
{
    cout << "Running TSFD main function with CYCLIC_CG" << endl;


    //std::ifstream domainfile("/tmp/pddl2sas_input_domain.pddl");
    //std::ifstream problemfile("/tmp/pddl2sas_input_problem.pddl");

    std::ifstream domainfile("/tmp/domain.txt");
    std::ifstream problemfile("/tmp/problem.txt");

    std::string domain((std::istreambuf_iterator<char>(domainfile)), (std::istreambuf_iterator<char>()));
    std::string problem((std::istreambuf_iterator<char>(problemfile)), (std::istreambuf_iterator<char>()));

    tsfd::TemporalStressingPlannerController pc;

    //pc.setPlannerProperty("heuristic", "makespan_preferred");
    pc.setPlannerProperty("anytime", "true");
    pc.setPlannerProperty("timeout_with_plan", "200");
    pc.setPlannerProperty("timeout_without_plan", "200");
    pc.setPlannerProperty("heuristic", "cyclic_cg_preferred");
    //pc.setPlannerProperty("heuristic", "known_plan_heuristic");
    //pc.setPlannerProperty("heuristic", "cyclic_cg");
    //pc.setPlannerProperty("heuristic", "rtpg");
    //pc.setPlannerProperty("heuristic", "rtpg_preferred");
    //pc.setPlannerProperty("anytime", "true");
    //pc.setPlannerProperty("heuristic", "no");
    //pc.setPlannerProperty("discretization", "2");
    pc.setPlannerProperty("epsilonize_internally", "true");
    //pc.setPlannerProperty("maximum makespan",  "700");

    //pc.setPlannerProperty("maximum cost", "0.6");
    //pc.setPlannerProperty("g_value_evaluation", "cost");
    pc.setPlannerProperty("cost calculation", "mul");

    pc.defineDomain(domain);
    pc.defineDomainProblems("test-problem", problem);
    pc.setPlanProblem("test-problem");

    pc.buildPlan();

    std::vector<PlanStep> plan = pc.getPlan();

    cout << "Plan length: " << plan.size() << endl;

    double overall_risk = 0;
    double overall_success = 1;
    for(const auto& step : plan) {
        overall_risk = 1 - (1 - overall_risk) * (1 - step.cost);
        overall_success = overall_success * (1 - step.cost);
    }
    cout << "Overall cost: " << overall_risk << endl;
    cout << "Overall succ: " << overall_success << " (" << (1 - overall_success) << ")" << endl;

    /*
    cout << "Known plan export for h:" << endl;
    for(const auto& step : plan) {
        cout << "optimal_plan.push_back(\"" << step.op->get_initial_name() << "\");" << endl;
    }

    /*
    TimeStampedState curr = pc.getPlanState();
    PlanStep curr_op = pc.getPlanAction();

    bool isApplicable = pc.checkPlanActionPreconditions(curr);
    cout << "Is applicable: " << isApplicable << endl;

    TimeStampedStateChange change = pc.getPlanActionEffects(curr);
    change.dump();

    // Apply op on state:
    curr = TimeStampedState(curr, *(curr_op.op));
    curr = curr.let_time_pass(curr.next_happening() - curr.timestamp, true);
    pc.advancePlan();

    curr_op = pc.getPlanAction();
    isApplicable = pc.checkPlanActionPreconditions(curr);
    cout << "Is applicable: " << isApplicable << endl;

    change = pc.getPlanActionEffects(curr);
    change.dump();
    */

    //double pc_best_makespan = pc.bestMakespan;


    /*cout << "====================================" << endl;
    cout << "Running TSFD main function with RTPG" << endl;

    tsfd::TemporalStressingPlannerController pc2;

    //pc2.setPlannerProperty("heuristic", "no");
    //pc2.setPlannerProperty("heuristic", "makespan");
    //pc2.setPlannerProperty("heuristic", "cyclic_cg");
    //pc2.setPlannerProperty("heuristic", "cyclic_cg_preferred");
    //pc2.setPlannerProperty("heuristic", "rtpg");
    pc2.setPlannerProperty("heuristic", "rtpg_preferred");
    pc2.setPlannerProperty("anytime", "true");
    //pc2.setPlannerProperty("heuristic", "no");
    //pc2.setPlannerProperty("concurrent_mode", "15");
    //pc2.setPlannerProperty("ordered_mode", "15");
    //pc2.setPlannerProperty("cheapest_mode", "15");
    //pc2.setPlannerProperty("discretization", "10");
    //pc2.setPlannerProperty("reschedule","true");
    pc2.setPlannerProperty("epsilonize_internally", "true");
    pc2.defineDomain(domain);
    pc2.defineDomainProblems("test-problem", problem);
    pc2.setPlanProblem("test-problem");

    pc2.buildPlan();

    double pc2_best_makespan = pc2.bestMakespan;

    cout << "DIFF: " << pc_best_makespan - pc2_best_makespan << endl;

    string sas_filepath = "/tmp/temporal_fast_downward_input.sas";
    ifstream in(sas_filepath);
    g_read_everything(in);
    g_dump_everything();

    PDDLAtom pddl;
    pddl.predicate = "handEmpty";
    pddl.args.push_back("testAgent__1");
    pddl.args.push_back("handright3a__52");

    cout << "PDDL to search" << endl;
    pddl.dump();
    SASAtom sas = pc.getSASForPDDLFact(pddl);
    cout << "SAS (not neg)" << endl;
    sas.dump();
    cout << "Single: " << sas.singleSAS() << ", Valid: " << sas.valid() << endl;

    pddl.negated = true;
    SASAtom sas2 = pc.getSASForPDDLFact(pddl);
    cout << "SAS (neg)" << endl;
    sas2.dump();

    SASAtom sas3;
    sas3.index = 5;
    sas3.values.push_back(0);

    cout << "SAS to search" << endl;
    sas3.dump();
    PDDLAtom pddl2 = pc.getPDDLForSASVariable(sas3);
    cout << "PDDL (not neg)" << endl;
    pddl2.dump();
    cout << "Negated: " << pddl2.negated << ", Valid: " << pddl2.valid() << endl;

    sas3.values.clear();
    sas3.values.push_back(1);
    PDDLAtom pddl3 = pc.getPDDLForSASVariable(sas3);
    pddl3.dump();
    cout << "Negated: " << pddl3.negated << ", Valid: " << pddl3.valid() << endl;*/
}
