//
//  pddl_to_sas_manager.cpp
//  TemporalConcurrentPlanner
//
//  Created by Fabian Peller on 28.02.18.
//  Copyright © 2018 Fabian Peller. All rights reserved.
//

#include "pddl_to_sas_manager.h"

#include <cassert>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

#include "globals.h"

using namespace std;
using namespace tsfd;

PDDLAtom::PDDLAtom(bool neg, const string& predicate, const vector<string>& args) :
    negated(neg), predicate(predicate), args(args)
{}

PDDLAtom::PDDLAtom(istream& in) :
    negated(false)
{
    in >> this->predicate;

    int num_args;
    in >> num_args;

    for(int i = 0; i < num_args; ++i) {
        string arg = "";
        in >> arg;

        assert(arg.length() > 2);
        arg = arg.substr(1, arg.size() - 2);

        this->args.push_back(arg);
    }
}

PDDLAtom::PDDLAtom(const PDDLAtom& a) :
    negated(a.negated), predicate(a.predicate), args(a.args)
{}

bool PDDLAtom::valid() const {
    return this->predicate != "";
}

string PDDLAtom::toString() const {
    stringstream ss;
    if(this->negated)
        ss << "not ";
    ss << this->predicate;
    for(const auto& arg : this->args)
        ss << " " << arg;

    return ss.str();
}

void PDDLAtom::dump() const {
    cout << this->toString() << endl;
}

SASAtom::SASAtom(int index, int v) :
    index(index), value(v)
{}

SASAtom::SASAtom(istream& in) : index(-1)
{
    std::string var_num;
    in >> var_num;

    // The groups file may contain removed variables.
    // if e.g. variable 29 is removed, then index 29 points to var30
    for(unsigned int i = 0; i < g_variable_name.size(); ++i) {
        if(g_variable_name[i] == "var"+var_num) {
            this->index = i;
            break;
        }
    }
    in >> this->value;
}

SASAtom::SASAtom(const SASAtom& a) :
    index(a.index), value(a.value)
{}

bool SASAtom::valid() const {
    return this->index != SASAtom::NO_VARIABLE;
}

string SASAtom::toString() const
{
    stringstream ss;
    ss << this->index << " " << this->value;
    return ss.str();
}
void SASAtom::dump() const {
    cout << this->toString() << endl;
}


PddlToSasManager::PddlToSasManager(istream& in)
{
    assert(g_variable_name.size() > 0);

    check_magic(in, "begin_groups");
    int groups_count;
    in >> groups_count;


    for(int i = 0; i < groups_count; ++i) {
        check_magic(in, "group");

        int group_count;
        in >> group_count;

        for(int j = 0; j < group_count; ++j) {
            SASAtom sas = SASAtom(in);
            PDDLAtom pddl = PDDLAtom(in);

            if(!sas.valid()) {
                // Found a removed variable (removed during preprocessing)
                continue;
            }

            this->insertPddlToSas(pddl, sas);
            this->insertSasToPddl(sas, pddl);

            // If there is only one entry in a group, the last entry is always the negation of those
            // After this call the loop always ends
            if(group_count == 1 && g_variable_domain[sas.index] != -1) {
                SASAtom neg_sas = SASAtom(sas);
                PDDLAtom neg_pddl = PDDLAtom(pddl);

                neg_sas.value = g_variable_domain[neg_sas.index] -1; // set value to negation (last in domain)
                neg_pddl.negated = true;

                this->insertPddlToSas(neg_pddl, neg_sas);
                this->insertSasToPddl(neg_sas, neg_pddl);

                break; // assert to end here
            }
        }


    }
    check_magic(in, "end_groups");
}

SASAtom PddlToSasManager::getSasForPddl(const PDDLAtom& pddl) const
{
    auto it = pddl_to_sas.find(pddl);
    if (it != pddl_to_sas.end()) {
        return (it->second);
    }

    return SASAtom();
}

PDDLAtom PddlToSasManager::getPddlForSas(const SASAtom& sas) const
{
    auto it = sas_to_pddl.find(sas);
    if (it != sas_to_pddl.end()) {
        return (it->second);
    }

    return PDDLAtom();
}

void PddlToSasManager::insertPddlToSas(const PDDLAtom& pddl, const SASAtom& sas) {
    this->pddl_to_sas.insert(pair<PDDLAtom, SASAtom>(pddl, sas));
}

void PddlToSasManager::insertSasToPddl(const SASAtom& sas, const PDDLAtom& pddl) {
    this->sas_to_pddl.insert(pair<SASAtom, PDDLAtom>(sas, pddl));
}

void PddlToSasManager::dump() const {
    cout << "PDDL 2 SAS" << endl;
    for(const auto& ent : pddl_to_sas) {
        cout << ent.first.toString() << " references to " << ent.second.toString() << endl;
    }

    cout << "SAS 2 PDDL" << endl;
    for(const auto& ent : sas_to_pddl) {
        cout << ent.first.toString() << " references to " << ent.second.toString() << endl;
    }
}
