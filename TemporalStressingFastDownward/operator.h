
#pragma once

#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <math.h>

#include "globals.h"
#include "state.h"


namespace tsfd {

class Operator
{
private:
    int index;
    string initial_name;
    string name;
    vector<Prevail> prevail_start; // var, val
    vector<Prevail> prevail_overall; // var, val
    vector<Prevail> prevail_end; // var, val
    vector<PrePost> pre_post_start; // var, old-val, new-val
    vector<PrePost> pre_post_end; // var, old-val, new-val
    int duration_var;

public:
        string costfunction;

        double used_acceleration;
        double initial_duration = 1;
        double initial_cost = 1;

private:
        bool deletesPrecond(const vector<Prevail>& conds,
                const vector<PrePost>& effects) const;
        bool deletesPrecond(const vector<PrePost>& effs1,
                const vector<PrePost>& effs2) const;
        bool achievesPrecond(const vector<PrePost>& effects, const vector<
                Prevail>& conds) const;
        bool achievesPrecond(const vector<PrePost>& effs1,
                const vector<PrePost>& effs2) const;
        bool writesOnSameVar(const vector<PrePost>& conds,
                const vector<PrePost>& effects) const;

    public:
        // create a useless operator
        Operator() : index(-1), name(""), duration_var(-1), costfunction("1"), used_acceleration(1)
        {}

        Operator(int, std::istream &in);
        explicit Operator(bool uses_concrete_time_information);
        void dump() const;
        const vector<Prevail> &get_prevail_start() const {
            return prevail_start;
        }
        const vector<Prevail> &get_prevail_overall() const {
            return prevail_overall;
        }
        const vector<Prevail> &get_prevail_end() const {
            return prevail_end;
        }
        const vector<PrePost> &get_pre_post_start() const {
            return pre_post_start;
        }
        const vector<PrePost> &get_pre_post_end() const {
            return pre_post_end;
        }
        const int &get_duration_var() const {
            return duration_var;
        }
        const string &get_name() const {
            return name;
        }
        const string &get_initial_name() const {
            return initial_name;
        }
        const int &get_index() const {
            return index;
        }

        bool operator<(const Operator &other) const;

        /// Calculate the duration of this operator when applied in state.
        /**
         * This function will retrieve the duration from state.
         */
        double get_duration(const TimeStampedState* state) const;

        void set_initial_cost(const TimeStampedState* init);
        void set_initial_duration(const TimeStampedState* init);

        double get_cost(const TimeStampedState* state) const;

        /// Compute applicability of this operator in state.
        /**
         * \param [out] timedSymbolicStates if not NULL the timedSymbolicStates will be computed
         */
        bool is_applicable(const TimeStampedState & state) const;

        bool isDisabledBy(const Operator* other) const;

        bool enables(const Operator* other) const;

        virtual ~Operator()
        {
        }
};

class ScheduledOperator : public Operator
{
    public:
        double time_increment;
        double started_in;
        double ends_in;
        double used_duration;
        double used_cost;

        ScheduledOperator(double t, double s, double e, double d, double c, const Operator& op) : Operator(op), time_increment(t), started_in(s), ends_in(e), used_duration(d), used_cost(c)
        {
        }
        ScheduledOperator(double t, double s,  double e, double d, double c) : Operator(true), time_increment(t), started_in(s), ends_in(e), used_duration(d), used_cost(c)
        {
            if (time_increment >= HUGE_VAL) {
                printf("WARNING: Created scheduled operator with time_increment %f\n", t);
            }
        }

        bool operator==(const ScheduledOperator& other) const {
            // We are lazy here...
            if(this->get_name() != other.get_name())
                return false;

            if(this->time_increment != other.time_increment)
                return false;
            if(this->started_in != other.started_in)
                return false;
            if(this->ends_in != other.ends_in)
                return false;
            if(this->used_duration != other.used_duration)
                return false;
            if(this->used_cost != other.used_cost)
                return false;
            return true;
        }
};
}
