//
//  real_time_stamped_state_change.hpp
//  TemporalConcurrentPlanner
//
//  Created by Fabian Peller on 28.02.18.
//  Copyright © 2018 Fabian Peller. All rights reserved.
//

#pragma once

#include <cassert>
#include <iostream>
#include <vector>
#include <memory>

#include "operator.h"
#include "state.h"


namespace tsfd {

class TimeStampedStateChange
{
public:
    TimeStampedStateChange() {}
    TimeStampedStateChange(const TimeStampedState&, const TimeStampedState&);
    TimeStampedStateChange(const TimeStampedStateChange&);

    ~TimeStampedStateChange()
    {}


    std::vector<PrePost> stateChanges;

    std::vector<ScheduledOperator> addedScheduledOperators;
    std::vector<ScheduledOperator> removedScheduledOperators;

    double timestamp_a;
    double timestamp_b;

    double cost_a;
    double cost_b;

    void dump() {
        cout << "TimeStampedStateChange:" << endl;
        for(const auto& pp : stateChanges) {
            cout << "Var" << pp.var << ": " << pp.pre << " =>" << pp.post << endl;
        }

        for(const auto& as : addedScheduledOperators) {
            cout << "+ " << as.get_name() << endl;
        }
        for(const auto& ds : removedScheduledOperators) {
            cout << "- " << ds.get_name() << endl;
        }

        cout << "TS: " << timestamp_a << " => " << timestamp_b << endl;
        cout << "Cost: " << cost_a << " => " << cost_b << endl;
    }

    // TODO: What about other members?
};

}
