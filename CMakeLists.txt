# Project TemporalStressedFastDownward
cmake_minimum_required(VERSION 2.8.12)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR}/CMakeModules)


###########################################################
#### Project configuration                             ####
###########################################################


project(TemporalStressingFastDownward)
set(CMAKE_PROJECT_EXECUTABLE_NAME
    "${CMAKE_PROJECT_NAME}Executable"
)

set(PROJ_VERSION 0.0.1)
set(PROJ_SO_VERSION 1) # shared lib (.so file) build number
set(CMAKE_CXX_STANDARD 11)

set(INTERNAL_INCLUDE_DIRS ${INTERNAL_INCLUDE_DIRS})


###########################################################
#### Configure and add files                           ####
###########################################################


add_subdirectory(TemporalStressingFastDownward) # define header and source files

set(TEMPORALSTRESSEDFASTDOWNWARD_BASE_DIR
        ${PROJECT_SOURCE_DIR}
)


###########################################################
#### CMake package configuration                       ####
###########################################################


set(EXTERNAL_INCLUDE_DIRS ${EXTERNAL_INCLUDE_DIRS})
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS})

#find_package(PythonLibs REQUIRED)
#set(EXTERNAL_INCLUDE_DIRS ${EXTERNAL_INCLUDE_DIRS} ${PYTHON_INCLUDE_DIRS})
#set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} ${PYTHON_LIBRARIES})


###########################################################
#### Compiler configuration                            ####
###########################################################

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall")

include_directories(SYSTEM ${EXTERNAL_INCLUDE_DIRS})
include_directories(PRIVATE ${INTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

set(CMAKE_POSITION_INDEPENDENT_CODE ON) # enable -fPIC

if(MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4 /MP")
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wno-long-long -pedantic")
endif()


###########################################################
#### Project build configuration                       ####
###########################################################

add_library(${CMAKE_PROJECT_NAME} SHARED ${LIB_SOURCES} ${LIB_HEADERS})
target_link_libraries(${CMAKE_PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS})

set(CMAKE_BUILD_DIR ${CMAKE_SOURCE_DIR}/build)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BUILD_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${CMAKE_BUILD_DIR})


# this allows dependant targets to automatically add include-dirs by simply linking against this project
target_include_directories(${CMAKE_PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>
)

# this allows to add 3rdParty libs to this project
#target_link_libraries(${CMAKE_PROJECT_NAME} PUBLIC ${CMAKE_DL_LIBS})
#target_include_directories(${CMAKE_PROJECT_NAME} PUBLIC
#    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/TSFD/3rdParty>
#    $<INSTALL_INTERFACE:include/TSFD/3rdParty>
#)

install(
    TARGETS "${CMAKE_PROJECT_NAME}"
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    INCLUDES DESTINATION include
)

install(FILES ${LIB_HEADERS} DESTINATION include/TemporalStressingFastDownward COMPONENT Devel)

export(
    TARGETS "${CMAKE_PROJECT_NAME}"
    FILE "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}Targets.cmake"
)

configure_file(
    "CMakeModules/${CMAKE_PROJECT_NAME}Config.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}Config.cmake"
    COPYONLY
)

set(INSTALL_CMAKE_DIR "share/cmake/${CMAKE_PROJECT_NAME}")
install(
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    FILE "${CMAKE_PROJECT_NAME}Targets.cmake"
    DESTINATION ${INSTALL_CMAKE_DIR}
)
install(
    FILES
        "CMakeModules/${CMAKE_PROJECT_NAME}Config.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake"
    DESTINATION ${INSTALL_CMAKE_DIR}
    COMPONENT Devel
)

export(PACKAGE ${CMAKE_PROJECT_NAME})

add_executable(${CMAKE_PROJECT_EXECUTABLE_NAME} ${MAIN_SOURCE})
target_link_libraries(${CMAKE_PROJECT_EXECUTABLE_NAME} PUBLIC ${CMAKE_PROJECT_NAME})
export(PACKAGE ${CMAKE_PROJECT_EXECUTABLE_NAME})

###########################################################
#### Version configuration                             ####
###########################################################


set_property(TARGET ${CMAKE_PROJECT_NAME} PROPERTY VERSION ${PROJ_VERSION})
set_property(TARGET ${CMAKE_PROJECT_NAME} PROPERTY SOVERSION ${PROJ_SO_VERSION})


include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake"
    VERSION ${PROJ_VERSION}
    COMPATIBILITY AnyNewerVersion
)

