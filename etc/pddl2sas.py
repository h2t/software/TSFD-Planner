#! /usr/bin/env python
import subprocess
import sys
import shutil
import os

def main():
	filepath = os.path.dirname(os.path.realpath(__file__))

	def run(*args, **kwargs):
		input = kwargs.pop("input", None)
		assert not kwargs
		redirections = {}
		if input:
			redirections["stdin"] = open(input)
		print args, redirections
		subprocess.check_call(sum([arg.split("+") for arg in args],[]), **redirections)

	domain, problem = sys.argv[1:]

	# run translator
	run(filepath+"/Pddl2SasTranslator/translate.py", domain, problem)

	# run preprocessing
	run(filepath+"/Pddl2SasPreprocessor/preprocess", input="/tmp/output.sas")

if __name__ == "__main__":
    main()

